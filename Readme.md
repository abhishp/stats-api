# stats-api

A simple API to serve simplified summary statistics (mean, min and max) on the ingested dataset.
It implements an in-memory data store for records.

## Features

- Add a record to the dataset
- Delete a record from the dataset.
- Fetch summary statistics for salary over the entire dataset.
- Fetch summary statistics for contractual workers.
- Fetch summary statistics for each department.
- Fetch summary statistics for each department and sub-department.

## Local Setup

- Install [golang v1.19](https://golang.org/dl/)
- Install [make](https://www.gnu.org/software/make/)


### Building the binary

Run the following command to build the code

```shell
$ make build 
```

### Starting the API server

```shell
$ make server
```

This would start the server on default port `3000`. If you want to start the server on a specific port you can setup the port and other configs as per the [configs](#configs) section.
The build binary (default at `out/bin/stats-api`) supports the following flags
- `-c`, `--config-file` <config-file> : specify the path to the config file
- `-e`, `--environment` <env> : specify the environment to start the app server in

### Configs
The configs can be set as either environment variables or using the configuration file. If a config is set in both 
environment variables and config file the environment config is given precedence.

A sample config file is provided as `config.sample.yml` which can readily be used to run the server by running `make copy-config`

| Environment                            | Config File Keys             | Description                                | Default          |
|----------------------------------------|------------------------------|--------------------------------------------|------------------|
| STATS_API_AUTH_TOKEN_SECRET_KEY        | AUTH_TOKEN.SECRET_KEY        | secret key to sign authentication token    | 256 random bytes |
| STATS_API_AUTH_TOKEN_VALIDITY_DURATION | AUTH_TOKEN.VALIDITY_DURATION | duration for authentication token validity | 15m              |
| STATS_API_HTTP_PORT                    | HTTP_PORT                    | API server port number                     | 3000             |
| STATS_API_USERS_FILE_PATH              | USERS_FILE_PATH              | file path for valid users in yaml format   |                  |

#### Config file
Sample config file: 
```yaml
HTTP_PORT: 3000
AUTH_TOKEN:
  VALIDITY_DURATION: 15m
  SECRET_KEY: some-super-secret-key
USERS_FILE_PATH: users.yml
```
Config files support environment wise overrides which can be specified as:

```yaml
HTTP_PORT: 3000
AUTH_TOKEN:
  VALIDITY_DURATION: 15m
  SECRET_KEY: some-super-secret-key
USERS_FILE_PATH: users.yml

development:
  HTTP_PORT: 3001
  USERS_FILE_PATH: users.dev.yml

test:
  HTTP_PORT: 3002
  AUTH_TOKEN:
    VALIDITY_DURATION: 1m
```
Based on the environment in which the app starts the respective configs would be overridden if there are any overrides. 
For example if `environment` is set to `development` the `HTTP_PORT` and `USERS_FILE_PATH` would be overridden.

## Development

### Tasks

- Fetch dependencies

  `make deps`
- Format code

  `make fmt`
- Run lint checks

  `make lint`

### Testing

- Run unit tests

  `make test-unit`

- Run integration tests
  - [Install newman](https://learning.postman.com/docs/running-collections/using-newman-cli/installing-running-newman/)
  - Run postman collection using newman
    
    `newman run stats-api.postman_collection.json --env-var "api-host=<host-for-stats-api>"`

### Docker

- Run the server and integration tests against the same.
  
  `docker compose up -d`

## API

All the APIs can be run using `Postman` tool and the provided postman collection.
Follow the following instructions to run the collection (including integration specs)
- Install [Postman](https://learning.postman.com/docs/getting-started/installation-and-updates/)
- [Import the collection](https://learning.postman.com/docs/getting-started/importing-and-exporting-data/) in the postman
- Execute the `Login` request in `Stats API` collection.
- Execute any other request.

### Login
Authenticate user using username and password combo. The resulting token can be used for authentication.
For clients like apps/browsers and postman it also puts the auth token as a cookie in response.
```shell
curl --location --request POST 'http://localhost:3000/login' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data-raw '{
    "username": "john.doe",
    "password": "some-super-secret-password"
}'
```

### Create Record
Create a new record and add it to the dataset.
```shell
curl --location --request POST 'http://localhost:3000/records' \
--header 'Authorization: Bearer <token from login response>' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Abhishek",
    "salary": "145000",
    "currency": "USD",
    "department": "Engineering",
    "sub_department": "Platform"
}'
```

### Delete Record
Delete a record from the dataset.
```shell
curl --location --request DELETE 'http://localhost:3000/records/{record-id}' \
--header 'Authorization: Bearer <token from login response>' \
--header 'Accept: application/json'
```

### List Records
List all the records in the dataset. It also supports filtering the records based on the `on_contract` query param which accepts a boolean value.
```shell
curl --location --request GET 'http://localhost:3000/records' \
--header 'Authorization: Bearer <token from login response>' \
--header 'Accept: application/json'
```

### Summary
This api calculates simplified summary based on the search and grouping criteria.
It supports the following query params

| Query Param | Values                        | Description                                                                                                                                       |
|-------------|-------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| on_contract | true<br/>false                | If present it will filter the records on the on_contract values before calculating the summary. Without this param set, all the records are used. |
| group_by    | department<br/>sub_department | If present it will aggregate the record statistics at the given level. Without this param set it returns summary over all dataset.                |

### Overall Simplified Summary
Fetch simplified summary for salary over the entire dataset.
```shell
curl --location --request GET 'http://localhost:3000/stats/summary' \
--header 'Authorization: Bearer <token from login response>' \
--header 'Accept: application/json'
```

### On Contract Simplified Summary
Fetch simplified summary for salary for records which satisfy `"on_contract": "true"` condition.
```shell
curl --location --request GET 'http://localhost:3000/stats/summary?on_contract=true' \
--header 'Authorization: Bearer <token from login response>' \
--header 'Accept: application/json'
```

### Simplified Summary for each department
Fetch simplified summary for salary for each department. 
It returns one simplified summary for each department and aggregate the records for each department.
```shell
curl --location --request GET 'http://localhost:3000/stats/summary?group_by=department' \
--header 'Authorization: Bearer <token from login response>' \
--header 'Accept: application/json'
```

### Simplified Summary for each department and sub department combination
Fetch simplified summary for salary for each department and sub department combination. It calculates simplified summary
for each sub department within each department.
```shell
curl --location --request GET 'http://localhost:3000/stats/summary?group_by=sub_department' \
--header 'Authorization: Bearer <token from login response>' \
--header 'Accept: application/json'
```

## Future Improvements

- Swagger documentation for API.
- Distributed tracing support for requests.
- Structured logging
- Add a persistent data store like `PostgreSQL` or `MongoDB`
- Add a write-through cache for record statistics to pre-calculate overall, on contract and departmental summaries as and when the data is ingested. 

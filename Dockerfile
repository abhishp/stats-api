# syntax=docker/dockerfile:1

FROM golang:1.19-alpine AS builder
COPY . /app
WORKDIR /app
RUN apk add --update --no-cache \
      bash \
      make && \
    make test-unit build

ENV PORT 3000

FROM alpine:3.16

EXPOSE ${PORT}

COPY --from=builder /app/out/bin/stats-api /stats-api/stats-api
COPY --from=builder /app/users.sample.yml /stats-api/users.yml

CMD ["/stats-api/stats-api", "-e", "development"]

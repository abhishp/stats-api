package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/pkg/errors"
	"github.com/urfave/cli/v2"
	"github.com/urfave/negroni"

	"gitlab.com/abhishp/stats-api/config"
	"gitlab.com/abhishp/stats-api/internal/authentication"
	"gitlab.com/abhishp/stats-api/internal/repository"
	"gitlab.com/abhishp/stats-api/internal/server"
	"gitlab.com/abhishp/stats-api/internal/service"
	repo "gitlab.com/abhishp/stats-api/internal/service/repository"
)

func main() {
	app := cli.NewApp()
	app.Name = "stats-api"
	app.Description = "API to serve summary statistics"
	app.DefaultCommand = "server"
	app.Flags = []cli.Flag{
		&cli.StringFlag{Name: "environment", Aliases: []string{"e"}, Usage: "app environment to start the API server in", EnvVars: []string{"STATS_API_ENVIRONMENT"}, Value: "development"},
		&cli.StringFlag{Name: "config-file", Aliases: []string{"c"}, Usage: "file path for config file", EnvVars: []string{"STATS_API_CONFIG_FILE_PATH"}, TakesFile: true},
	}

	app.Commands = cli.Commands{{
		Name:   "server",
		Usage:  "Start stats API web server",
		Action: startAPIServer,
	}}
	if err := app.Run(os.Args); err != nil {
		log.Fatalln("failed to boot stats API: ", err)
	}
}

func startAPIServer(appCtx *cli.Context) error {
	apiConfig, err := config.LoadConfig(appCtx.String("environment"), appCtx.String("config-file"))
	if err != nil {
		return err
	}
	middlewares := negroni.New(negroni.NewRecovery())
	middlewares.Use(negroni.NewLogger())
	usersYAMLFile, err := os.OpenFile(apiConfig.UsersFilePath, os.O_RDONLY, os.ModePerm)
	if err != nil {
		return errors.Wrap(err, "failed to open users file")
	}

	userRepository, err := repository.NewUserYAMLRepository(usersYAMLFile)
	if err != nil {
		return err
	}
	repos := repo.Registry{
		Record: repository.NewRecordInMemoryRepository(),
		User:   userRepository,
	}
	services := service.NewRegistry(repos)
	router := server.InitializeRouter(services, authentication.NewService(services.User, apiConfig.AuthToken))
	middlewares.UseHandler(router)
	apiServer := &http.Server{Addr: ":" + strconv.Itoa(apiConfig.HTTPPort), Handler: middlewares}
	go listenAndServe(apiServer)
	fmt.Println("Booted stats API server")
	return waitForShutdown(apiServer)
}

func listenAndServe(apiServer *http.Server) {
	if err := apiServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		fmt.Println("server startup failure: ", err)
	}
}

func waitForShutdown(apiServer *http.Server) error {
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	sig := <-sigChan
	fmt.Println("Received signal: ", sig.String())
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := apiServer.Shutdown(ctx); err != nil {
		return err
	}
	fmt.Println("stats API server shut down.")
	return nil
}

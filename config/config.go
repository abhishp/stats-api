package config

import (
	"reflect"
	"strings"
	"time"

	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"github.com/samber/lo"
	"github.com/spf13/viper"
)

type APIConfig struct {
	HTTPPort      int             `conf:"HTTP_PORT"`
	UsersFilePath string          `conf:"USERS_FILE_PATH"`
	AuthToken     AuthTokenConfig `conf:"AUTH_TOKEN"`
}

type AuthTokenConfig struct {
	ValidityDuration time.Duration `conf:"VALIDITY_DURATION"`
	SecretKey        []byte        `conf:"SECRET_KEY"`
}

func LoadConfig(environment string, configFilePaths ...string) (APIConfig, error) {
	conf := viper.New()
	conf.SetEnvPrefix("STATS_API")
	if len(configFilePaths) > 0 {
		conf.SetConfigFile(configFilePaths[0])
	} else {
		conf.SetConfigName("config")
		conf.SetConfigType("yml")
		conf.AddConfigPath(".")
	}
	conf.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	conf.AutomaticEnv()
	bindEnvVariables(conf)
	if err := conf.ReadInConfig(); err != nil && !errors.As(err, &viper.ConfigFileNotFoundError{}) {
		return APIConfig{}, err
	}

	if environment == "" {
		environment = "dev"
	}
	if envConfig := conf.GetStringMap(environment); len(envConfig) != 0 {
		if err := conf.MergeConfigMap(envConfig); err != nil {
			return APIConfig{}, err
		}
	}
	apiConfig := defaultConfig()

	err := conf.Unmarshal(&apiConfig, func(config *mapstructure.DecoderConfig) {
		config.TagName = "conf"
		config.ZeroFields = true
		config.DecodeHook = mapstructure.ComposeDecodeHookFunc(stringToByteSliceHook, config.DecodeHook)
	})
	if err != nil {
		return APIConfig{}, err
	}
	return apiConfig, nil
}

func defaultConfig() APIConfig {
	return APIConfig{
		HTTPPort:  3000,
		AuthToken: AuthTokenConfig{ValidityDuration: 15 * time.Minute, SecretKey: []byte(lo.RandomString(256, lo.AllCharset))},
	}
}

func bindEnvVariables(conf *viper.Viper) {
	conf.MustBindEnv("AUTH_TOKEN.SECRET_KEY")
	conf.MustBindEnv("AUTH_TOKEN.VALIDITY_DURATION")
	conf.MustBindEnv("HTTP_PORT")
	conf.MustBindEnv("USERS_FILE_PATH")
}

func stringToByteSliceHook(f reflect.Type, t reflect.Type, data any) (any, error) {
	if f.Kind() != reflect.String || (t.Kind() != reflect.Slice || t.Elem().Kind() != reflect.Uint8) {
		return data, nil
	}

	return []byte(data.(string)), nil
}

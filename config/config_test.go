package config_test

import (
	"os"
	"path"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/abhishp/stats-api/config"
)

func TestLoadConfig(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		createTempConfigFile(t, "config.yml", "HTTP_PORT: 3000\ndev:\n  HTTP_PORT: 3002\ntest:\n  HTTP_PORT: 3003")

		t.Run("without explicit app environment", func(t *testing.T) {
			t.Run("no env variables override", func(t *testing.T) {
				apiConfig, err := config.LoadConfig("")

				assert.NoError(t, err)
				assert.Equal(t, 3002, apiConfig.HTTPPort)
			})

			t.Run("env variables override", func(t *testing.T) {
				t.Setenv("STATS_API_HTTP_PORT", "3008")

				apiConfig, err := config.LoadConfig("")

				assert.NoError(t, err)
				assert.Equal(t, 3008, apiConfig.HTTPPort)
			})
		})

		t.Run("with explicit app environment", func(t *testing.T) {
			t.Run("no env variables override", func(t *testing.T) {
				apiConfig, err := config.LoadConfig("test")

				assert.NoError(t, err)
				assert.Equal(t, 3003, apiConfig.HTTPPort)
			})

			t.Run("env variables override", func(t *testing.T) {
				t.Setenv("STATS_API_HTTP_PORT", "3007")

				apiConfig, err := config.LoadConfig("test")

				assert.NoError(t, err)
				assert.Equal(t, 3007, apiConfig.HTTPPort)
			})
		})

		t.Run("when config file path is given", func(t *testing.T) {
			configFilePath := path.Join(t.TempDir(), "config.yml")
			createTempConfigFile(t, configFilePath, "HTTP_PORT: 5000")
			apiConfig, err := config.LoadConfig("test", configFilePath)

			assert.NoError(t, err)
			assert.Equal(t, 5000, apiConfig.HTTPPort)
		})
	})

	t.Run("error", func(t *testing.T) {
		t.Run("invalid config value", func(t *testing.T) {
			createTempConfigFile(t, "config.yml", "")
			t.Setenv("STATS_API_HTTP_PORT", "port_number")

			apiConfig, err := config.LoadConfig("")

			assert.Regexp(t, `cannot parse 'HTTP_PORT' as int: strconv.ParseInt: parsing "port_number": invalid syntax`, err.Error())
			assert.Empty(t, apiConfig)
		})
	})
}

func TestAuthTokenValidityDuration(t *testing.T) {
	createTempConfigFile(t, "config.yml", "AUTH_TOKEN:\n  VALIDITY_DURATION: 2m")
	t.Run("success", func(t *testing.T) {
		apiConfig, err := config.LoadConfig("")

		assert.NoError(t, err)
		assert.Equal(t, 2*time.Minute, apiConfig.AuthToken.ValidityDuration)
	})

	t.Run("error", func(t *testing.T) {
		t.Setenv("STATS_API_AUTH_TOKEN_VALIDITY_DURATION", "invalid")

		apiConfig, err := config.LoadConfig("")

		assert.Error(t, err)
		assert.Regexp(t, `error decoding 'AUTH_TOKEN.VALIDITY_DURATION': time: invalid duration "invalid"`, err.Error())
		assert.Empty(t, apiConfig)
	})
}

func TestAuthTokenSecretKey(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		t.Setenv("STATS_API_AUTH_TOKEN_SECRET_KEY", "a836bdb192a84510a8b19256ad042cd1c40cc708500c4000985449169a435113")
		apiConfig, err := config.LoadConfig("")

		assert.NoError(t, err)
		assert.Equal(t, []byte("a836bdb192a84510a8b19256ad042cd1c40cc708500c4000985449169a435113"), apiConfig.AuthToken.SecretKey)
	})
}

func TestUsersFilePath(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		createTempConfigFile(t, "config.yml", "USERS_FILE_PATH: /path/to/users.yml")
		apiConfig, err := config.LoadConfig("")

		assert.NoError(t, err)
		assert.Equal(t, "/path/to/users.yml", apiConfig.UsersFilePath)
	})
}

func createTempConfigFile(t *testing.T, configFilePath string, configs string) {
	t.Helper()
	require.NoError(t, os.WriteFile(configFilePath, []byte(configs), os.ModePerm))
	t.Cleanup(func() { _ = os.Remove(configFilePath) })
}

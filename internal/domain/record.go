package domain

import "github.com/gofrs/uuid"

type Record struct {
	ID            uuid.UUID `json:"id"`
	Name          string    `json:"name"`
	Salary        int64     `json:"salary"`
	Currency      string    `json:"currency"`
	Department    string    `json:"department"`
	SubDepartment string    `json:"sub_department"`
	OnContract    bool      `json:"on_contract,omitempty"`
}

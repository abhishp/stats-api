package domain

type SimplifiedSummary struct {
	Mean float64 `json:"mean"`
	Min  int64   `json:"min"`
	Max  int64   `json:"max"`
}

package domain

import "github.com/gofrs/uuid"

type User struct {
	ID             uuid.UUID
	Name           string
	Username       string
	HashedPassword string `yaml:"password"`
}

package service

type Error struct {
	code int
	msg  string
}

func (e Error) Error() string {
	return e.msg
}

func (e Error) Code() int {
	return e.code
}

var (
	ErrUserNotFound     = Error{code: 1, msg: "user not found"}
	ErrRecordNotFound   = Error{code: 2, msg: "record not found"}
	ErrNoRecordsPresent = Error{code: 3, msg: "no records present"}
)

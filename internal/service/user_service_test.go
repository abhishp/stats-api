package service_test

import (
	"context"
	"errors"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/service"
	"gitlab.com/abhishp/stats-api/internal/service/repository"
	"gitlab.com/abhishp/stats-api/internal/testing/factory"
	"gitlab.com/abhishp/stats-api/internal/testing/mocks"
)

func TestNewUserService(t *testing.T) {
	userService := service.NewUserService(mocks.NewUserRepository(t))

	assert.NotNil(t, userService)
	assert.Implements(t, (*service.UserService)(nil), userService)
}

func TestUserService_GetUserByUsername(t *testing.T) {
	ctx := context.Background()
	t.Run("success", func(t *testing.T) {
		user := factory.Users().Build()

		mockUserRepo := mocks.NewUserRepository(t)
		userService := service.NewUserService(mockUserRepo)

		mockUserRepo.EXPECT().GetUserByUsername(ctx, user.Username).Return(user, nil)

		svcUser, err := userService.GetUserByUsername(ctx, user.Username)

		assert.NoError(t, err)
		assert.Equal(t, user, svcUser)
	})

	t.Run("errors", func(t *testing.T) {
		repoError := errors.New("some user repo error")
		testCases := []struct {
			name                     string
			repoError, expectedError error
		}{
			{name: "user not found", repoError: repository.ErrNotFound, expectedError: service.ErrUserNotFound},
			{name: "some user repo error", repoError: repoError, expectedError: repoError},
		}
		for _, testCase := range testCases {
			t.Run(testCase.name, func(t *testing.T) {
				mockUserRepo := mocks.NewUserRepository(t)
				userService := service.NewUserService(mockUserRepo)
				username := gofakeit.Username()

				mockUserRepo.EXPECT().GetUserByUsername(ctx, username).Return(domain.User{}, testCase.repoError)

				user, err := userService.GetUserByUsername(ctx, username)

				assert.ErrorIs(t, err, testCase.expectedError)
				assert.Empty(t, user)
			})
		}
	})
}

package repository

import (
	"context"

	"github.com/gofrs/uuid"

	"gitlab.com/abhishp/stats-api/internal/domain"
)

type RecordRepository interface {
	Delete(ctx context.Context, id uuid.UUID) error
	GetByID(ctx context.Context, id uuid.UUID) (domain.Record, error)
	Insert(ctx context.Context, record domain.Record) error
	List(ctx context.Context, searchParams RecordSearchParams) ([]domain.Record, error)
}

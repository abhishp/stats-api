package repository

type RecordSearchParams struct {
	OnContract *bool // nil: no effect, true/false: fetch records with respective on_contract value
}

package repository

import (
	"context"

	"gitlab.com/abhishp/stats-api/internal/domain"
)

type UserRepository interface {
	GetUserByUsername(ctx context.Context, username string) (domain.User, error)
}

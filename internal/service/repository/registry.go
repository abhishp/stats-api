package repository

type Registry struct {
	Record RecordRepository
	User   UserRepository
}

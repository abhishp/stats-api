package contract

import (
	"gopkg.in/validator.v2"
)

type ValidationError struct {
	msg string
}

func (v ValidationError) Error() string {
	return v.msg
}

func validateStruct(s any) error {
	if err := validator.Validate(s); err != nil {
		return ValidationError{msg: err.Error()}
	}
	return nil
}

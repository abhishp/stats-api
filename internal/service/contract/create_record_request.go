package contract

import "github.com/pkg/errors"

type CreateRecordRequest struct {
	Name          string `json:"name" validate:"nonzero"`
	Salary        string `json:"salary" validate:"nonzero,regexp=^[1-9][0-9]+$"`
	Currency      string `json:"currency" validate:"nonzero"`
	Department    string `json:"department" validate:"nonzero"`
	SubDepartment string `json:"sub_department" validate:"nonzero"`
	OnContract    string `json:"on_contract" validate:"regexp=^(true|false)?$"`
}

func (crr CreateRecordRequest) Validate() error {
	return errors.Wrap(validateStruct(crr), "create record request validation failure")
}

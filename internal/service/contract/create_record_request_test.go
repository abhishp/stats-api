package contract_test

import (
	"fmt"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/service/contract"
)

func TestCreateRecordRequest_Validate(t *testing.T) {
	validRequest := contract.CreateRecordRequest{
		Name:          gofakeit.Name(),
		Salary:        gofakeit.Numerify("#########"),
		Currency:      gofakeit.CurrencyShort(),
		Department:    gofakeit.JobTitle(),
		SubDepartment: gofakeit.JobDescriptor(),
		OnContract:    fmt.Sprint(gofakeit.Bool()),
	}

	t.Run("success", func(t *testing.T) {
		err := validRequest.Validate()

		assert.NoError(t, err)
	})

	t.Run("errors", func(t *testing.T) {
		t.Run("name", func(t *testing.T) {
			request := validRequest
			request.Name = ""

			err := request.Validate()

			assert.EqualError(t, err, "create record request validation failure: Name: zero value")
		})

		t.Run("salary", func(t *testing.T) {
			t.Run("missing", func(t *testing.T) {
				request := validRequest
				request.Salary = ""

				err := request.Validate()

				assert.EqualError(t, err, "create record request validation failure: Salary: zero value, regular expression mismatch")
			})

			t.Run("invalid value", func(t *testing.T) {
				request := validRequest
				request.Salary = "abc"

				err := request.Validate()

				assert.EqualError(t, err, "create record request validation failure: Salary: regular expression mismatch")
			})
		})

		t.Run("currency", func(t *testing.T) {
			request := validRequest
			request.Currency = ""

			err := request.Validate()

			assert.EqualError(t, err, "create record request validation failure: Currency: zero value")
		})

		t.Run("department", func(t *testing.T) {
			request := validRequest
			request.Department = ""

			err := request.Validate()

			assert.EqualError(t, err, "create record request validation failure: Department: zero value")
		})

		t.Run("sub department", func(t *testing.T) {
			request := validRequest
			request.SubDepartment = ""

			err := request.Validate()

			assert.EqualError(t, err, "create record request validation failure: SubDepartment: zero value")
		})

		t.Run("on contract", func(t *testing.T) {
			request := validRequest
			request.OnContract = "non-bool"

			err := request.Validate()

			assert.EqualError(t, err, "create record request validation failure: OnContract: regular expression mismatch")
		})
	})
}

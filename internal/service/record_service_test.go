package service_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/service"
	"gitlab.com/abhishp/stats-api/internal/service/contract"
	"gitlab.com/abhishp/stats-api/internal/service/repository"
	"gitlab.com/abhishp/stats-api/internal/testing/factory"
	"gitlab.com/abhishp/stats-api/internal/testing/matchers"
	"gitlab.com/abhishp/stats-api/internal/testing/mocks"
)

func TestNewRecordService(t *testing.T) {
	recordService := service.NewRecordService(mocks.NewRecordRepository(t))

	assert.NotNil(t, recordService)
	assert.Implements(t, (*service.RecordService)(nil), recordService)
}

func TestRecordService_Insert(t *testing.T) {
	ctx := context.Background()
	t.Run("success", func(t *testing.T) {
		mockRecordRepo := mocks.NewRecordRepository(t)
		recordService := service.NewRecordService(mockRecordRepo)

		record := factory.Records().Build()
		mockRecordRepo.EXPECT().Insert(ctx, matchers.Records(t, record, false)).Return(nil)
		request := contract.CreateRecordRequest{
			Name:          record.Name,
			Salary:        fmt.Sprint(record.Salary),
			Currency:      record.Currency,
			Department:    record.Department,
			SubDepartment: record.SubDepartment,
			OnContract:    fmt.Sprint(record.OnContract),
		}

		svcRecord, err := recordService.Insert(ctx, request)

		assert.NoError(t, err)
		matchers.AssertEqualRecords(t, record, svcRecord, false)
	})

	t.Run("error", func(t *testing.T) {
		t.Run("validation failure", func(t *testing.T) {
			mockRecordRepo := mocks.NewRecordRepository(t)
			recordService := service.NewRecordService(mockRecordRepo)

			svcRecord, err := recordService.Insert(ctx, contract.CreateRecordRequest{})

			assert.Error(t, err)
			assert.Empty(t, svcRecord)
			assert.ErrorAs(t, err, &contract.ValidationError{})
		})

		t.Run("repo error", func(t *testing.T) {
			mockRecordRepo := mocks.NewRecordRepository(t)
			recordService := service.NewRecordService(mockRecordRepo)

			record := factory.Records().Build()
			recordRepoError := errors.New("some record repo error")
			mockRecordRepo.EXPECT().Insert(ctx, matchers.Records(t, record, false)).Return(recordRepoError)
			request := contract.CreateRecordRequest{
				Name:          record.Name,
				Salary:        fmt.Sprint(record.Salary),
				Currency:      record.Currency,
				Department:    record.Department,
				SubDepartment: record.SubDepartment,
				OnContract:    fmt.Sprint(record.OnContract),
			}

			svcRecord, err := recordService.Insert(ctx, request)

			assert.ErrorIs(t, err, recordRepoError)
			assert.Empty(t, svcRecord)
		})
	})
}

func TestRecordService_Delete(t *testing.T) {
	ctx := context.Background()
	testCases := map[string]error{
		"success":    nil,
		"repo error": errors.New("some record repo delete error"),
	}
	for name, svcError := range testCases {
		t.Run(name, func(t *testing.T) {
			mockRecordRepo := mocks.NewRecordRepository(t)
			recordService := service.NewRecordService(mockRecordRepo)

			recordID := uuid.Must(uuid.NewV4())
			mockRecordRepo.EXPECT().Delete(ctx, recordID).Return(svcError)

			err := recordService.Delete(ctx, recordID)

			assert.Equal(t, svcError, err)
		})
	}

	t.Run("record not found", func(t *testing.T) {
		mockRecordRepo := mocks.NewRecordRepository(t)
		recordService := service.NewRecordService(mockRecordRepo)

		recordID := uuid.Must(uuid.NewV4())
		mockRecordRepo.EXPECT().Delete(ctx, recordID).Return(repository.ErrNotFound)

		err := recordService.Delete(ctx, recordID)

		assert.ErrorIs(t, err, service.ErrRecordNotFound)
	})
}

func TestRecordService_List(t *testing.T) {
	ctx := context.Background()
	testCases := []struct {
		name    string
		error   error
		records []domain.Record
	}{
		{name: "success", records: factory.Records().BuildN(5)},
		{name: "error", error: errors.New("some error from repo")},
	}

	for _, testCase := range testCases {
		t.Run(testCase.name, func(t *testing.T) {
			mockRecordRepo := mocks.NewRecordRepository(t)
			recordService := service.NewRecordService(mockRecordRepo)

			request := contract.RecordSearchRequest{OnContract: lo.Sample([]*bool{nil, lo.ToPtr(true), lo.ToPtr(false)})}
			mockRecordRepo.EXPECT().List(ctx, repository.RecordSearchParams{OnContract: request.OnContract}).Return(testCase.records, testCase.error)

			svcRecords, err := recordService.List(ctx, request)

			assert.Equal(t, testCase.error, err)
			assert.Equal(t, testCase.records, svcRecords)
		})
	}
}

package service

import (
	"context"
	"math"

	"github.com/samber/lo"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/service/contract"
)

type StatsService interface {
	GetSalarySummary(ctx context.Context, filterParams contract.RecordSearchRequest) (domain.SimplifiedSummary, error)
	GetSalarySummaryByDepartments(ctx context.Context, filterParams contract.RecordSearchRequest) (map[string]domain.SimplifiedSummary, error)
	GetSalarySummaryBySubDepartments(ctx context.Context, filterParams contract.RecordSearchRequest) (map[string]map[string]domain.SimplifiedSummary, error)
}

func NewStatsService(recordService RecordService) StatsService {
	return statsService{recordService: recordService}
}

type statsService struct {
	recordService RecordService
}

func (s statsService) GetSalarySummary(ctx context.Context, filterParams contract.RecordSearchRequest) (domain.SimplifiedSummary, error) {
	records, err := s.fetchAllRecords(ctx, filterParams)
	if err != nil {
		return domain.SimplifiedSummary{}, err
	}
	return calculateSimplifiedSummary(records), nil
}

func (s statsService) GetSalarySummaryByDepartments(ctx context.Context, filterParams contract.RecordSearchRequest) (map[string]domain.SimplifiedSummary, error) {
	records, err := s.fetchAllRecords(ctx, filterParams)
	if err != nil {
		return nil, err
	}
	groupedRecords := lo.GroupBy(records, func(record domain.Record) string {
		return record.Department
	})
	departmentalSummary := make(map[string]domain.SimplifiedSummary, len(groupedRecords))
	for department, deptRecords := range groupedRecords {
		departmentalSummary[department] = calculateSimplifiedSummary(deptRecords)
	}
	return departmentalSummary, nil
}

func (s statsService) GetSalarySummaryBySubDepartments(ctx context.Context, filterParams contract.RecordSearchRequest) (map[string]map[string]domain.SimplifiedSummary, error) {
	records, err := s.fetchAllRecords(ctx, filterParams)
	if err != nil {
		return nil, err
	}
	departmentalRecords := lo.GroupBy(records, func(record domain.Record) string {
		return record.Department
	})
	subDepartmentalSummary := lo.MapValues(departmentalRecords, func(records []domain.Record, _ string) map[string]domain.SimplifiedSummary {
		subDepartmentalRecords := lo.GroupBy(records, func(record domain.Record) string {
			return record.SubDepartment
		})
		subDepartmentalSummary := make(map[string]domain.SimplifiedSummary, len(subDepartmentalRecords))
		for subDepartment, subDeptRecords := range subDepartmentalRecords {
			subDepartmentalSummary[subDepartment] = calculateSimplifiedSummary(subDeptRecords)
		}
		return subDepartmentalSummary
	})

	return subDepartmentalSummary, nil
}

func (s statsService) fetchAllRecords(ctx context.Context, filterParams contract.RecordSearchRequest) ([]domain.Record, error) {
	records, err := s.recordService.List(ctx, filterParams)
	if err != nil {
		return nil, err
	}
	if len(records) == 0 {
		return nil, ErrNoRecordsPresent
	}
	return records, nil
}

func calculateSimplifiedSummary(records []domain.Record) domain.SimplifiedSummary {
	ss := domain.SimplifiedSummary{}
	ss.Min = records[0].Salary
	for idx, record := range records {
		if ss.Min > record.Salary {
			ss.Min = record.Salary
		}
		if ss.Max < record.Salary {
			ss.Max = record.Salary
		}
		ss.Mean += (float64(record.Salary) - ss.Mean) / float64(idx+1)
	}
	ss.Mean = math.Round(ss.Mean*1000_000) / 1000_000 // round off mean value to 6 decimal places
	return ss
}

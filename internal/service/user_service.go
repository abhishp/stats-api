package service

import (
	"context"
	"errors"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/service/repository"
)

type UserService interface {
	GetUserByUsername(ctx context.Context, username string) (domain.User, error)
}

type userService struct {
	userRepo repository.UserRepository
}

func NewUserService(userRepo repository.UserRepository) UserService {
	return userService{userRepo: userRepo}
}

func (u userService) GetUserByUsername(ctx context.Context, username string) (domain.User, error) {
	user, err := u.userRepo.GetUserByUsername(ctx, username)
	if errors.Is(err, repository.ErrNotFound) {
		return domain.User{}, ErrUserNotFound
	}

	return user, err
}

package service

import (
	"context"
	"strconv"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/service/contract"
	"gitlab.com/abhishp/stats-api/internal/service/repository"
)

type RecordService interface {
	Delete(ctx context.Context, id uuid.UUID) error
	Insert(ctx context.Context, request contract.CreateRecordRequest) (domain.Record, error)
	List(ctx context.Context, searchParams contract.RecordSearchRequest) ([]domain.Record, error)
}

func NewRecordService(recordRepo repository.RecordRepository) RecordService {
	return &recordService{recordRepo: recordRepo}
}

type recordService struct {
	recordRepo repository.RecordRepository
}

func (r recordService) Delete(ctx context.Context, id uuid.UUID) error {
	err := r.recordRepo.Delete(ctx, id)
	if errors.Is(err, repository.ErrNotFound) {
		return ErrRecordNotFound
	}
	return err
}

func (r recordService) Insert(ctx context.Context, request contract.CreateRecordRequest) (domain.Record, error) {
	if err := request.Validate(); err != nil {
		return domain.Record{}, err
	}

	id, err := uuid.NewV4()
	if err != nil {
		return domain.Record{}, errors.Wrap(err, "failed to create new UUID")
	}
	salary, err := strconv.ParseInt(request.Salary, 10, 64)
	if err != nil {
		return domain.Record{}, errors.Wrap(err, "failed to parse salary as number")
	}
	record := domain.Record{
		ID:            id,
		Name:          request.Name,
		Salary:        salary,
		Currency:      request.Currency,
		Department:    request.Department,
		SubDepartment: request.SubDepartment,
	}
	if request.OnContract != "" {
		record.OnContract, err = strconv.ParseBool(request.OnContract)
		if err != nil {
			return domain.Record{}, errors.Wrap(err, "failed to parse on_contract field as bool")
		}
	}

	if err = r.recordRepo.Insert(ctx, record); err != nil {
		return domain.Record{}, err
	}
	return record, nil
}

func (r recordService) List(ctx context.Context, request contract.RecordSearchRequest) ([]domain.Record, error) {
	return r.recordRepo.List(ctx, repository.RecordSearchParams(request))
}

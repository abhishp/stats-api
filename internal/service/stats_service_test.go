package service_test

import (
	"context"
	"errors"
	"testing"

	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/service"
	"gitlab.com/abhishp/stats-api/internal/service/contract"
	"gitlab.com/abhishp/stats-api/internal/testing/factory"
	"gitlab.com/abhishp/stats-api/internal/testing/mocks"
)

var testRecords = []domain.Record{{
	Name:          "Abhishek",
	Salary:        145_000,
	Currency:      "USD",
	Department:    "Engineering",
	SubDepartment: "Platform",
}, {
	Name:          "Anurag",
	Salary:        90_000,
	Currency:      "USD",
	Department:    "Banking",
	OnContract:    true,
	SubDepartment: "Loan",
}, {
	Name:          "Himani",
	Salary:        240_000,
	Currency:      "USD",
	Department:    "Engineering",
	SubDepartment: "Customer",
}, {
	Name:          "Yatendra",
	Salary:        30,
	Currency:      "USD",
	Department:    "Operations",
	SubDepartment: "CustomerOnboarding",
}, {
	Name:          "Ragini",
	Salary:        30,
	Currency:      "USD",
	Department:    "Engineering",
	SubDepartment: "Platform",
}, {
	Name:          "Nikhil",
	Salary:        110_000,
	Currency:      "USD",
	OnContract:    true,
	Department:    "Engineering",
	SubDepartment: "Platform",
}, {
	Name:          "Guljit",
	Salary:        30,
	Currency:      "USD",
	Department:    "Administration",
	SubDepartment: "Agriculture",
}, {
	Name:          "Himanshu",
	Salary:        70_000,
	Currency:      "EUR",
	Department:    "Operations",
	SubDepartment: "CustomerOnboarding",
}, {
	Name:          "Anupam",
	Salary:        200_000_000,
	Currency:      "INR",
	Department:    "Engineering",
	SubDepartment: "Customer",
}}

func TestNewStatsService(t *testing.T) {
	statsService := service.NewStatsService(mocks.NewRecordService(t))

	assert.NotNil(t, statsService)
	assert.Implements(t, (*service.StatsService)(nil), statsService)
}

func TestStatsService_GetSalaryStats(t *testing.T) {
	ctx := context.Background()
	t.Run("success", func(t *testing.T) {
		testCases := map[string]*bool{
			"without on_contract search param":           nil,
			"with on_contract search param set to true":  lo.ToPtr(true),
			"with on_contract search param set to false": lo.ToPtr(false),
		}
		for name, onContract := range testCases {
			t.Run(name, func(t *testing.T) {
				mockRecordService := mocks.NewRecordService(t)
				statsService := service.NewStatsService(mockRecordService)
				salaries := []int64{20, 10, 50, 30, 40}

				recordBuilder := factory.Records()
				if onContract != nil {
					recordBuilder.OnContract(*onContract)
				}
				records := make([]domain.Record, 0, len(salaries))
				for _, salary := range salaries {
					records = append(records, recordBuilder.Salary(salary).Build())
				}

				searchParams := contract.RecordSearchRequest{OnContract: onContract}
				mockRecordService.EXPECT().List(ctx, searchParams).Return(records, nil)
				expectedSummaryStats := domain.SimplifiedSummary{
					Mean: 30.0,
					Min:  10,
					Max:  50,
				}

				stats, err := statsService.GetSalarySummary(ctx, searchParams)

				assert.NoError(t, err)
				assert.Equal(t, expectedSummaryStats, stats)
			})
		}
	})

	t.Run("error", func(t *testing.T) {
		t.Run("service failure", func(t *testing.T) {
			mockRecordService := mocks.NewRecordService(t)
			statsService := service.NewStatsService(mockRecordService)

			searchParams := contract.RecordSearchRequest{}
			recordServiceError := errors.New("some record service error")
			mockRecordService.EXPECT().List(ctx, searchParams).Return(nil, recordServiceError)

			simplifiedSummary, err := statsService.GetSalarySummary(ctx, searchParams)

			assert.ErrorIs(t, err, recordServiceError)
			assert.Empty(t, simplifiedSummary)
		})

		t.Run("no records present", func(t *testing.T) {
			mockRecordService := mocks.NewRecordService(t)
			statsService := service.NewStatsService(mockRecordService)

			searchParams := contract.RecordSearchRequest{}
			mockRecordService.EXPECT().List(ctx, searchParams).Return([]domain.Record{}, nil)

			simplifiedSummary, err := statsService.GetSalarySummary(ctx, searchParams)

			assert.ErrorIs(t, err, service.ErrNoRecordsPresent)
			assert.Empty(t, simplifiedSummary)
		})
	})
}

func TestStatsService_GetSalarySummaryByDepartments(t *testing.T) {
	ctx := context.Background()
	t.Run("success", func(t *testing.T) {
		testCases := []struct {
			name         string
			records      []domain.Record
			onContract   *bool
			summaryStats map[string]domain.SimplifiedSummary
		}{{
			name:       "without on_contract search param",
			records:    testRecords,
			onContract: nil,
			summaryStats: map[string]domain.SimplifiedSummary{
				"Engineering":    {Mean: 40_099_006, Min: 30, Max: 200_000_000},
				"Banking":        {Mean: 90_000, Min: 90_000, Max: 90_000},
				"Operations":     {Mean: 35_015, Min: 30, Max: 70000},
				"Administration": {Mean: 30, Min: 30, Max: 30},
			},
		}, {
			name:       "with on_contract search param set to true",
			records:    lo.Filter(testRecords, func(r domain.Record, _ int) bool { return r.OnContract }),
			onContract: lo.ToPtr(true),
			summaryStats: map[string]domain.SimplifiedSummary{
				"Engineering": {Mean: 110_000, Min: 110_000, Max: 110_000},
				"Banking":     {Mean: 90_000, Min: 90_000, Max: 90_000},
			},
		}, {
			name:       "with on_contract search param set to false",
			records:    lo.Filter(testRecords, func(r domain.Record, _ int) bool { return !r.OnContract }),
			onContract: lo.ToPtr(false),
			summaryStats: map[string]domain.SimplifiedSummary{
				"Engineering":    {Mean: 50_096_257.5, Min: 30, Max: 200_000_000},
				"Operations":     {Mean: 35_015, Min: 30, Max: 70000},
				"Administration": {Mean: 30, Min: 30, Max: 30},
			},
		}}
		for _, testCase := range testCases {
			t.Run(testCase.name, func(t *testing.T) {
				mockRecordService := mocks.NewRecordService(t)
				statsService := service.NewStatsService(mockRecordService)
				searchParams := contract.RecordSearchRequest{OnContract: testCase.onContract}
				mockRecordService.EXPECT().List(ctx, searchParams).Return(testCase.records, nil)

				stats, err := statsService.GetSalarySummaryByDepartments(ctx, searchParams)

				assert.NoError(t, err)
				assert.Equal(t, testCase.summaryStats, stats)
			})
		}
	})

	t.Run("error", func(t *testing.T) {
		t.Run("service failure", func(t *testing.T) {
			mockRecordService := mocks.NewRecordService(t)
			statsService := service.NewStatsService(mockRecordService)

			searchParams := contract.RecordSearchRequest{}
			recordServiceError := errors.New("some record service error")
			mockRecordService.EXPECT().List(ctx, searchParams).Return(nil, recordServiceError)

			simplifiedSummary, err := statsService.GetSalarySummaryByDepartments(ctx, searchParams)

			assert.ErrorIs(t, err, recordServiceError)
			assert.Nil(t, simplifiedSummary)
		})

		t.Run("no records present", func(t *testing.T) {
			mockRecordService := mocks.NewRecordService(t)
			statsService := service.NewStatsService(mockRecordService)

			searchParams := contract.RecordSearchRequest{}
			mockRecordService.EXPECT().List(ctx, searchParams).Return([]domain.Record{}, nil)

			simplifiedSummary, err := statsService.GetSalarySummaryByDepartments(ctx, searchParams)

			assert.ErrorIs(t, err, service.ErrNoRecordsPresent)
			assert.Nil(t, simplifiedSummary)
		})
	})
}

func TestStatsService_GetSalarySummaryBySubDepartments(t *testing.T) {
	ctx := context.Background()
	t.Run("success", func(t *testing.T) {
		testCases := []struct {
			name         string
			records      []domain.Record
			onContract   *bool
			summaryStats map[string]map[string]domain.SimplifiedSummary
		}{{
			name:       "without on_contract search param",
			records:    testRecords,
			onContract: nil,
			summaryStats: map[string]map[string]domain.SimplifiedSummary{
				"Engineering": {
					"Platform": {Mean: 85_010, Min: 30, Max: 145_000},
					"Customer": {Mean: 100_120_000, Min: 240_000, Max: 200_000_000},
				},
				"Banking": {
					"Loan": {Mean: 90_000, Min: 90_000, Max: 90_000},
				},
				"Operations": {
					"CustomerOnboarding": {Mean: 35_015, Min: 30, Max: 70000},
				},
				"Administration": {
					"Agriculture": {Mean: 30, Min: 30, Max: 30},
				},
			},
		}, {
			name:       "with on_contract search param set to true",
			records:    lo.Filter(testRecords, func(r domain.Record, _ int) bool { return r.OnContract }),
			onContract: lo.ToPtr(true),
			summaryStats: map[string]map[string]domain.SimplifiedSummary{
				"Engineering": {
					"Platform": {Mean: 110_000, Min: 110_000, Max: 110_000},
				},
				"Banking": {
					"Loan": {Mean: 90_000, Min: 90_000, Max: 90_000},
				},
			},
		}, {
			name:       "with on_contract search param set to false",
			records:    lo.Filter(testRecords, func(r domain.Record, _ int) bool { return !r.OnContract }),
			onContract: lo.ToPtr(false),
			summaryStats: map[string]map[string]domain.SimplifiedSummary{
				"Engineering": {
					"Platform": {Mean: 72_515, Min: 30, Max: 145_000},
					"Customer": {Mean: 100_120_000, Min: 240_000, Max: 200_000_000},
				},
				"Operations": {
					"CustomerOnboarding": {Mean: 35_015, Min: 30, Max: 70000},
				},
				"Administration": {
					"Agriculture": {Mean: 30, Min: 30, Max: 30},
				},
			},
		}}
		for _, testCase := range testCases {
			t.Run(testCase.name, func(t *testing.T) {
				mockRecordService := mocks.NewRecordService(t)
				statsService := service.NewStatsService(mockRecordService)
				searchParams := contract.RecordSearchRequest{OnContract: testCase.onContract}
				mockRecordService.EXPECT().List(ctx, searchParams).Return(testCase.records, nil)

				stats, err := statsService.GetSalarySummaryBySubDepartments(ctx, searchParams)

				assert.NoError(t, err)
				assert.Equal(t, testCase.summaryStats, stats)
			})
		}
	})

	t.Run("error", func(t *testing.T) {
		t.Run("service failure", func(t *testing.T) {
			mockRecordService := mocks.NewRecordService(t)
			statsService := service.NewStatsService(mockRecordService)

			searchParams := contract.RecordSearchRequest{}
			recordServiceError := errors.New("some record service error")
			mockRecordService.EXPECT().List(ctx, searchParams).Return(nil, recordServiceError)

			simplifiedSummary, err := statsService.GetSalarySummaryBySubDepartments(ctx, searchParams)

			assert.ErrorIs(t, err, recordServiceError)
			assert.Nil(t, simplifiedSummary)
		})

		t.Run("no records present", func(t *testing.T) {
			mockRecordService := mocks.NewRecordService(t)
			statsService := service.NewStatsService(mockRecordService)

			searchParams := contract.RecordSearchRequest{}
			mockRecordService.EXPECT().List(ctx, searchParams).Return([]domain.Record{}, nil)

			simplifiedSummary, err := statsService.GetSalarySummaryBySubDepartments(ctx, searchParams)

			assert.ErrorIs(t, err, service.ErrNoRecordsPresent)
			assert.Nil(t, simplifiedSummary)
		})
	})
}

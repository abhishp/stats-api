package service

import "gitlab.com/abhishp/stats-api/internal/service/repository"

type Registry struct {
	Record RecordService
	Stats  StatsService
	User   UserService
}

func NewRegistry(repos repository.Registry) Registry {
	recordSvc := NewRecordService(repos.Record)
	return Registry{
		Record: recordSvc,
		Stats:  NewStatsService(recordSvc),
		User:   NewUserService(repos.User),
	}
}

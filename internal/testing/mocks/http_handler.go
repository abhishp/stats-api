// Code generated by mockery v2.14.0. DO NOT EDIT.

package mocks

import (
	http "net/http"

	mock "github.com/stretchr/testify/mock"
)

// HTTPHandler is an autogenerated mock type for the HTTPHandler type
type HTTPHandler struct {
	mock.Mock
}

type HTTPHandler_Expecter struct {
	mock *mock.Mock
}

func (_m *HTTPHandler) EXPECT() *HTTPHandler_Expecter {
	return &HTTPHandler_Expecter{mock: &_m.Mock}
}

// ServeHTTP provides a mock function with given fields: _a0, _a1
func (_m *HTTPHandler) ServeHTTP(_a0 http.ResponseWriter, _a1 *http.Request) {
	_m.Called(_a0, _a1)
}

// HTTPHandler_ServeHTTP_Call is a *mock.Call that shadows Run/Return methods with type explicit version for method 'ServeHTTP'
type HTTPHandler_ServeHTTP_Call struct {
	*mock.Call
}

// ServeHTTP is a helper method to define mock.On call
//  - _a0 http.ResponseWriter
//  - _a1 *http.Request
func (_e *HTTPHandler_Expecter) ServeHTTP(_a0 interface{}, _a1 interface{}) *HTTPHandler_ServeHTTP_Call {
	return &HTTPHandler_ServeHTTP_Call{Call: _e.mock.On("ServeHTTP", _a0, _a1)}
}

func (_c *HTTPHandler_ServeHTTP_Call) Run(run func(_a0 http.ResponseWriter, _a1 *http.Request)) *HTTPHandler_ServeHTTP_Call {
	_c.Call.Run(func(args mock.Arguments) {
		run(args[0].(http.ResponseWriter), args[1].(*http.Request))
	})
	return _c
}

func (_c *HTTPHandler_ServeHTTP_Call) Return() *HTTPHandler_ServeHTTP_Call {
	_c.Call.Return()
	return _c
}

type mockConstructorTestingTNewHTTPHandler interface {
	mock.TestingT
	Cleanup(func())
}

// NewHTTPHandler creates a new instance of HTTPHandler. It also registers a testing interface on the mock and a cleanup function to assert the mocks expectations.
func NewHTTPHandler(t mockConstructorTestingTNewHTTPHandler) *HTTPHandler {
	mock := &HTTPHandler{}
	mock.Mock.Test(t)

	t.Cleanup(func() { mock.AssertExpectations(t) })

	return mock
}

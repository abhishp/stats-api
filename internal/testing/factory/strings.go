package factory

import (
	"strings"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/samber/lo"
)

func Username() string {
	connector := lo.Sample([]string{"_", "."})
	return strings.ToLower(gofakeit.FirstName() + connector + gofakeit.LastName())
}

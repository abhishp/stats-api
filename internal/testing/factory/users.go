package factory

import (
	"github.com/brianvoe/gofakeit/v6"
	"github.com/gofrs/uuid"

	"gitlab.com/abhishp/stats-api/internal/domain"
)

func Users(traits ...trait[domain.User]) *userFactory {
	return &userFactory{factory[domain.User]{build: buildUser, traits: traits}}
}

func UsersFrom(user domain.User, traits ...trait[domain.User]) *userFactory {
	uf := Users(traits...)
	uf.obj = &user
	return uf
}

func buildUser() domain.User {
	return domain.User{
		ID:             uuid.Must(uuid.NewV4()),
		Username:       Username(),
		HashedPassword: gofakeit.Password(true, true, true, true, false, 15),
		Name:           gofakeit.Name(),
	}
}

type userFactory struct {
	factory[domain.User]
}

func (uf *userFactory) ID(id uuid.UUID) *userFactory {
	uf.override("ID", id)
	return uf
}

func (uf *userFactory) Name(name string) *userFactory {
	uf.override("Name", name)
	return uf
}

func (uf *userFactory) Username(username string) *userFactory {
	uf.override("Username", username)
	return uf
}

func (uf *userFactory) HashedPassword(hashedPassword string) *userFactory {
	uf.override("HashedPassword", hashedPassword)
	return uf
}

package factory

import (
	"reflect"

	"github.com/gofrs/uuid"
)

const attrNameID = "ID"

type trait[T any] func(*T)

type factory[T any] struct {
	obj       *T
	overrides map[string]any
	build     func() T
	traits    []trait[T]
}

func (f *factory[T]) override(name string, value any) {
	if f.overrides == nil {
		f.overrides = map[string]any{}
	}
	f.overrides[name] = value
}

func (f *factory[T]) Build() T {
	base := f.obj
	if f.obj == nil {
		obj := f.build()
		base = &obj
	}
	if len(f.traits) > 0 {
		for _, trait := range f.traits {
			trait(base)
		}
	}

	if len(f.overrides) == 0 {
		return *base
	}
	ele := reflect.ValueOf(base).Elem()
	for name, value := range f.overrides {
		field := ele.FieldByName(name)
		if field.IsValid() && field.CanSet() {
			field.Set(reflect.ValueOf(value))
		}
	}
	return *base
}

func (f *factory[T]) BuildN(n int) []T {
	objects := make([]T, 0, n)
	for i := 0; i < n; i++ {
		if _, isValid := reflect.TypeOf(reflect.ValueOf(f.obj).Elem()).FieldByName(attrNameID); isValid {
			f.override(attrNameID, uuid.Must(uuid.NewV4()))
		}
		objects = append(objects, f.Build())
	}
	return objects
}

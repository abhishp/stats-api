package factory

import (
	"math/rand"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/gofrs/uuid"

	"gitlab.com/abhishp/stats-api/internal/domain"
)

func Records(traits ...trait[domain.Record]) *recordFactory {
	return &recordFactory{factory[domain.Record]{build: buildRecord, traits: traits}}
}

func RecordsFrom(record domain.Record, traits ...trait[domain.Record]) *recordFactory {
	uf := Records(traits...)
	uf.obj = &record
	return uf
}

func buildRecord() domain.Record {
	return domain.Record{
		ID:            uuid.Must(uuid.NewV4()),
		Name:          gofakeit.Name(),
		Salary:        rand.Int63(),
		Currency:      gofakeit.CurrencyShort(),
		Department:    gofakeit.JobDescriptor(),
		SubDepartment: gofakeit.JobTitle(),
		OnContract:    gofakeit.Bool(),
	}
}

type recordFactory struct {
	factory[domain.Record]
}

func (rf *recordFactory) ID(id uuid.UUID) *recordFactory {
	rf.override("ID", id)
	return rf
}

func (rf *recordFactory) Name(name string) *recordFactory {
	rf.override("Name", name)
	return rf
}

func (rf *recordFactory) Salary(salary int64) *recordFactory {
	rf.override("Salary", salary)
	return rf
}

func (rf *recordFactory) Currency(currency string) *recordFactory {
	rf.override("Currency", currency)
	return rf
}

func (rf *recordFactory) Department(department string) *recordFactory {
	rf.override("Department", department)
	return rf
}

func (rf *recordFactory) SubDepartment(subDepartment string) *recordFactory {
	rf.override("SubDepartment", subDepartment)
	return rf
}

func (rf *recordFactory) OnContract(onContract bool) *recordFactory {
	rf.override("OnContract", onContract)
	return rf
}

package matchers

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/abhishp/stats-api/internal/domain"
)

func Records(t *testing.T, expected domain.Record, checkID bool) any {
	t.Helper()
	return mock.MatchedBy(func(actual domain.Record) bool {
		t.Helper()
		return AssertEqualRecords(t, expected, actual, checkID)
	})
}

func AssertEqualRecords(t *testing.T, expected, actual domain.Record, checkID bool) bool {
	t.Helper()
	result := assert.Equal(t, expected.Name, actual.Name, "name")
	result = assert.Equal(t, expected.Salary, actual.Salary, "salary") && result
	result = assert.Equal(t, expected.Currency, actual.Currency, "currency") && result
	result = assert.Equal(t, expected.Department, actual.Department, "department") && result
	result = assert.Equal(t, expected.SubDepartment, actual.SubDepartment, "sub department") && result
	result = assert.Equal(t, expected.OnContract, actual.OnContract, "on contract") && result
	if checkID {
		return assert.Equal(t, expected.ID, actual.ID, "id") && result
	}
	return result
}

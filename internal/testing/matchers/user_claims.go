package matchers

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/abhishp/stats-api/internal/authentication"
)

func UserClaims(t *testing.T, expected authentication.UserClaims) any {
	return mock.MatchedBy(func(req *http.Request) bool {
		context, err := authentication.ClaimsFromContext(req.Context())
		return assert.NoError(t, err) && assert.EqualValues(t, expected, context)
	})
}

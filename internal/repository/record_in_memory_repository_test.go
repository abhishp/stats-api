package repository_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/repository"
	svcrepo "gitlab.com/abhishp/stats-api/internal/service/repository"
	"gitlab.com/abhishp/stats-api/internal/testing/factory"
)

func TestNewRecordInMemoryRepository(t *testing.T) {
	recordRepo := repository.NewRecordInMemoryRepository()

	assert.NotNil(t, recordRepo)
	assert.Implements(t, (*svcrepo.RecordRepository)(nil), recordRepo)
}

func TestRecordInMemoryRepository_Insert(t *testing.T) {
	recordRepo := repository.NewRecordInMemoryRepository()
	ctx := context.Background()
	t.Run("success", func(t *testing.T) {
		record := factory.Records().Build()

		err := recordRepo.Insert(ctx, record)

		assert.NoError(t, err)
		repoRecord, err := recordRepo.GetByID(ctx, record.ID)
		assert.NoError(t, err)
		assert.Equal(t, record, repoRecord)
	})

	t.Run("errors", func(t *testing.T) {
		record := factory.Records().Build()
		assert.NoError(t, recordRepo.Insert(ctx, record))

		err := recordRepo.Insert(ctx, record)

		assert.ErrorIs(t, err, svcrepo.ErrUniqueViolation)
	})
}

func TestRecordInMemoryRepository_GetByID(t *testing.T) {
	recordRepo := repository.NewRecordInMemoryRepository()
	ctx := context.Background()
	record := factory.Records().Build()
	require.NoError(t, recordRepo.Insert(ctx, record))
	t.Run("success", func(t *testing.T) {
		repoRecord, err := recordRepo.GetByID(ctx, record.ID)

		assert.NoError(t, err)
		assert.Equal(t, record, repoRecord)
	})

	t.Run("errors", func(t *testing.T) {
		recordID := uuid.Must(uuid.NewV4())
		repoRecord, err := recordRepo.GetByID(ctx, recordID)

		assert.ErrorIs(t, err, svcrepo.ErrNotFound)
		assert.Empty(t, repoRecord)
	})
}

func TestRecordInMemoryRepository_Delete(t *testing.T) {
	recordRepo := repository.NewRecordInMemoryRepository()
	ctx := context.Background()
	record := factory.Records().Build()
	require.NoError(t, recordRepo.Insert(ctx, record))
	t.Run("success", func(t *testing.T) {
		err := recordRepo.Delete(ctx, record.ID)

		assert.NoError(t, err)
		rec, err := recordRepo.GetByID(ctx, record.ID)
		assert.ErrorIs(t, err, svcrepo.ErrNotFound)
		assert.Empty(t, rec)
	})

	t.Run("errors", func(t *testing.T) {
		recordID := uuid.Must(uuid.NewV4())
		err := recordRepo.Delete(ctx, recordID)

		assert.ErrorIs(t, err, svcrepo.ErrNotFound)
	})
}

func TestRecordInMemoryRepository_List(t *testing.T) {
	recordRepo := repository.NewRecordInMemoryRepository()
	ctx := context.Background()
	records := factory.Records().BuildN(10)
	for _, record := range records {
		require.NoError(t, recordRepo.Insert(ctx, record))
	}

	t.Run("success", func(t *testing.T) {
		t.Run("empty search params", func(t *testing.T) {
			svcRecords, err := recordRepo.List(ctx, svcrepo.RecordSearchParams{})

			assert.NoError(t, err)
			assert.ElementsMatch(t, records, svcRecords)
		})

		t.Run("on contract search param set", func(t *testing.T) {
			testCases := map[bool][]domain.Record{
				true:  lo.Filter(records, func(r domain.Record, _ int) bool { return r.OnContract }),
				false: lo.Filter(records, func(r domain.Record, _ int) bool { return !r.OnContract }),
			}
			for onContract, expectedRecords := range testCases {
				t.Run(fmt.Sprintf("%v", onContract), func(t *testing.T) {
					svcRecords, err := recordRepo.List(ctx, svcrepo.RecordSearchParams{OnContract: &onContract})

					assert.NoError(t, err)
					assert.ElementsMatch(t, expectedRecords, svcRecords)
				})
			}
		})
	})
}

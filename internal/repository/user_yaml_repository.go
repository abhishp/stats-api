package repository

import (
	"context"
	"io"

	"github.com/pkg/errors"
	"gopkg.in/yaml.v3"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/service/repository"
)

type userYAMLRepository struct {
	usersFile io.Reader
	users     map[string]domain.User
}

func NewUserYAMLRepository(usersYAMLFile io.Reader) (repository.UserRepository, error) {
	if usersYAMLFile == nil {
		return nil, errors.New("invalid users YAML file")
	}
	yamlRepository := userYAMLRepository{usersFile: usersYAMLFile}
	decoder := yaml.NewDecoder(usersYAMLFile)
	users := make([]domain.User, 0, 10)
	if err := decoder.Decode(&users); err != nil {
		return nil, errors.Wrap(err, "failed to raed users from YAML file")
	}
	yamlRepository.users = make(map[string]domain.User, len(users))
	for _, user := range users {
		if _, isPresent := yamlRepository.users[user.Username]; isPresent {
			return nil, errors.Wrap(repository.ErrUniqueViolation, "duplicate username "+user.Username)
		}
		yamlRepository.users[user.Username] = user
	}
	return yamlRepository, nil
}

func (u userYAMLRepository) GetUserByUsername(_ context.Context, username string) (domain.User, error) {
	user, isPresent := u.users[username]
	if !isPresent {
		return domain.User{}, repository.ErrNotFound
	}
	return user, nil
}

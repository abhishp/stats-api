package repository_test

import (
	"bytes"
	"context"
	"io"
	"os"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/repository"
	servicerepo "gitlab.com/abhishp/stats-api/internal/service/repository"
)

func TestNewUserYAMLRepository(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		buffer := bytes.NewBufferString(`- id: "cdd3faa3-9439-4c88-9105-1686980ba736"
  name: "John Doe"
  username: "john.doe"
  password: "&!@p8G3DP2AX54yyhv5ysYv%s^sr##RK_Wm5N*G"
`)

		userRepository, err := repository.NewUserYAMLRepository(buffer)

		assert.NoError(t, err)
		assert.NotNil(t, userRepository)
		assert.Implements(t, (*servicerepo.UserRepository)(nil), userRepository)
	})

	t.Run("errors", func(t *testing.T) {
		t.Run("valid yaml reader", func(t *testing.T) {
			testCases := []struct {
				name                 string
				usersYAML            string
				expectedErrorMessage string
			}{
				{name: "empty user yaml", expectedErrorMessage: "failed to raed users from YAML file: EOF"},
				{name: "invalid data type", usersYAML: "- id: invalid-uuid", expectedErrorMessage: "failed to raed users from YAML file: uuid: incorrect UUID length 12 in string \"invalid-uuid\""},
				{name: "duplicate username", usersYAML: "- id: 'f0033f65-c06e-44be-ae13-cfbd277a52a8'\n  username: john.doe\n- id: 'f0033f65-c06e-44be-ae13-cfbd277a52a8'\n  username: john.doe", expectedErrorMessage: "duplicate username john.doe: unique violation"},
			}
			for _, testCase := range testCases {
				t.Run(testCase.name, func(t *testing.T) {
					usersYAML := bytes.NewBufferString(testCase.usersYAML)

					userRepository, err := repository.NewUserYAMLRepository(usersYAML)

					assert.EqualError(t, err, testCase.expectedErrorMessage)
					assert.Nil(t, userRepository)
				})
			}
		})

		t.Run("invalid yaml reader", func(t *testing.T) {
			testCases := []struct {
				name                 string
				usersYAMLReader      io.Reader
				expectedErrorMessage string
			}{
				{name: "nil user yaml", expectedErrorMessage: "invalid users YAML file"},
				{name: "closed user yaml", usersYAMLReader: func() io.Reader {
					file, _ := os.CreateTemp(t.TempDir(), "users_*.yaml")
					_, _ = file.WriteString("- id: 'f0033f65-c06e-44be-ae13-cfbd277a52a8'\n  username: john.doe")
					_ = file.Close()
					return file
				}(), expectedErrorMessage: "failed to raed users from YAML file: yaml: input error: .* file already closed"},
			}

			for _, testCase := range testCases {
				t.Run(testCase.name, func(t *testing.T) {
					userRepository, err := repository.NewUserYAMLRepository(testCase.usersYAMLReader)

					assert.Regexp(t, testCase.expectedErrorMessage, err.Error())
					assert.Nil(t, userRepository)
				})
			}
		})
	})
}

func TestUserYAMLRepository_GetUserByUsername(t *testing.T) {
	buffer := bytes.NewBufferString(`- id: "cdd3faa3-9439-4c88-9105-1686980ba736"
  name: "John Doe"
  username: "john.doe"
  password: "&!@p8G3DP2AX54yyhv5ysYv%s^sr##RK_Wm5N*G"`)

	ctx := context.Background()
	userRepo, err := repository.NewUserYAMLRepository(buffer)
	require.NoError(t, err)

	t.Run("success", func(t *testing.T) {
		expectedUser := domain.User{
			ID:             uuid.FromStringOrNil("cdd3faa3-9439-4c88-9105-1686980ba736"),
			Name:           "John Doe",
			Username:       "john.doe",
			HashedPassword: "&!@p8G3DP2AX54yyhv5ysYv%s^sr##RK_Wm5N*G",
		}

		user, err := userRepo.GetUserByUsername(ctx, "john.doe")

		assert.NoError(t, err)
		assert.Equal(t, expectedUser, user)
	})

	t.Run("errors", func(t *testing.T) {
		user, err := userRepo.GetUserByUsername(ctx, "jane.doe")

		assert.ErrorIs(t, err, servicerepo.ErrNotFound)
		assert.Empty(t, user)
	})
}

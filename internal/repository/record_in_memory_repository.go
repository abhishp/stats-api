package repository

import (
	"context"
	"sync"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/samber/lo"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/service/repository"
)

func NewRecordInMemoryRepository() repository.RecordRepository {
	return &recordInMemoryRepository{
		records: map[uuid.UUID]domain.Record{},
	}
}

type recordInMemoryRepository struct {
	mutex   sync.Mutex
	records map[uuid.UUID]domain.Record
}

func (r *recordInMemoryRepository) Delete(_ context.Context, id uuid.UUID) error {
	if _, isPresent := r.records[id]; !isPresent {
		return errors.Wrap(repository.ErrNotFound, "record does not exist with id "+id.String())
	}
	r.mutex.Lock()
	defer r.mutex.Unlock()
	delete(r.records, id)
	return nil
}

func (r *recordInMemoryRepository) GetByID(_ context.Context, id uuid.UUID) (domain.Record, error) {
	record, isPresent := r.records[id]
	if isPresent {
		return record, nil
	}
	return record, errors.Wrap(repository.ErrNotFound, "record not found with id "+id.String())
}

func (r *recordInMemoryRepository) Insert(_ context.Context, record domain.Record) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	if _, isPresent := r.records[record.ID]; isPresent {
		return errors.Wrap(repository.ErrUniqueViolation, "record already exist with same id "+record.ID.String())
	}
	r.records[record.ID] = record
	return nil
}

func (r *recordInMemoryRepository) List(_ context.Context, searchParams repository.RecordSearchParams) ([]domain.Record, error) {
	if searchParams.OnContract == nil {
		return lo.Values(r.records), nil
	}
	result := make([]domain.Record, 0, len(r.records))
	for _, record := range r.records {
		if record.OnContract == *searchParams.OnContract {
			result = append(result, record)
		}
	}
	return result, nil
}

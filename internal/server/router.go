package server

import (
	"net/http"
	"net/http/pprof"

	"github.com/gorilla/mux"

	"gitlab.com/abhishp/stats-api/internal/authentication"
	"gitlab.com/abhishp/stats-api/internal/server/handler"
	"gitlab.com/abhishp/stats-api/internal/service"
)

func InitializeRouter(services service.Registry, authService authentication.Service) *mux.Router {
	router := mux.NewRouter()
	debugRouter(router)
	authHandler := authentication.NewHandler(authService)
	recordHandler := handler.NewRecordHandler(services.Record)
	statsHandler := handler.NewSummaryStatsHandler(services.Stats)

	router.HandleFunc("/health-check", handler.HealthCheck).Methods(http.MethodGet)
	router.HandleFunc("/login", authHandler.Login).Methods(http.MethodPost)

	// Authenticated routes
	authMiddleware := authentication.NewMiddleware(authService)
	authenticatedRouter := router.PathPrefix("/").Subrouter()
	authenticatedRouter.Use(authMiddleware)
	authenticatedRouter.HandleFunc("/records", recordHandler.Create).Methods(http.MethodPost)
	authenticatedRouter.HandleFunc("/records", recordHandler.List).Methods(http.MethodGet)
	authenticatedRouter.HandleFunc("/records/{id}", recordHandler.Delete).Methods(http.MethodDelete)
	authenticatedRouter.HandleFunc("/stats/summary", statsHandler.Index).Methods(http.MethodGet)

	return router
}

func debugRouter(mainRouter *mux.Router) {
	subRouter := mainRouter.PathPrefix("/debug/pprof").Subrouter()
	subRouter.HandleFunc("/cmdline", pprof.Cmdline)
	subRouter.HandleFunc("/profile", pprof.Profile)
	subRouter.HandleFunc("/symbol", pprof.Symbol)
	subRouter.HandleFunc("/trace", pprof.Trace)
	subRouter.HandleFunc("/{profile}", pprof.Index)
}

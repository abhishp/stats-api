package response

type Response struct {
	Success   bool   `json:"success"`
	ErrorCode int    `json:"errorCode,omitempty"`
	Message   string `json:"message,omitempty"`
}

func NewErrorResponse(code int, message string) Response {
	return Response{Success: false, ErrorCode: code, Message: message}
}

func NewSuccessResponse() Response {
	return Response{Success: true}
}

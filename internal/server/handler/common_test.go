package handler_test

import (
	"encoding/json"
	"io"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/server/response"
)

func assertErrorResponse(t *testing.T, expectedErrorCode int, expectedMessage string, responseBody io.Reader) bool {
	errorResponse := response.Response{}
	result := assert.NoError(t, json.NewDecoder(responseBody).Decode(&errorResponse))
	result = assert.False(t, errorResponse.Success) && result
	result = assert.Equal(t, expectedErrorCode, errorResponse.ErrorCode) && result
	return assert.Regexp(t, expectedMessage, errorResponse.Message) && result
}

func assertUnknownError(t *testing.T, responseBody io.Reader) bool {
	return assertErrorResponse(t, 500, "something went wrong", responseBody)
}

func assertSuccessResponse(t *testing.T, responseBody io.Reader) bool {
	successResponse := response.Response{}
	result := assert.NoError(t, json.NewDecoder(responseBody).Decode(&successResponse))
	return assert.Equal(t, response.Response{Success: true}, successResponse) && result
}

func testMissingBodyErrorWithRequest(handler http.HandlerFunc, r *http.Request) func(t *testing.T) {
	return func(t *testing.T) {
		t.Helper()
		w := httptest.NewRecorder()
		r.Body = nil
		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusBadRequest, w.Code)
		assertErrorResponse(t, 100, "bad request: missing body", w.Body)
	}
}

func testMissingBodyError(handler http.HandlerFunc, method, url string) func(t *testing.T) {
	r, _ := http.NewRequest(method, url, nil)
	return testMissingBodyErrorWithRequest(handler, r)
}

func testBodyDecodeErrorWithRequest(handler http.HandlerFunc, r *http.Request) func(t *testing.T) {
	return func(t *testing.T) {
		t.Helper()
		w := httptest.NewRecorder()
		r.Body = io.NopCloser(strings.NewReader("{"))

		handler.ServeHTTP(w, r)

		assert.Equal(t, http.StatusBadRequest, w.Code)
		assertErrorResponse(t, 101, "bad request: decode body: unexpected EOF", w.Body)
	}
}

func testBodyDecodeError(handler http.HandlerFunc, method, url string) func(t *testing.T) {
	r, _ := http.NewRequest(method, url, strings.NewReader("{"))
	return testBodyDecodeErrorWithRequest(handler, r)
}

package handler

import (
	"encoding/json"
	"net/http"
)

var healthCheckResponse = json.RawMessage(`{ "API": "OK" }`)

func HealthCheck(w http.ResponseWriter, _ *http.Request) {
	jsonResponse(w, http.StatusOK, healthCheckResponse)
}

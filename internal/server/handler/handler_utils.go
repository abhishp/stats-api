package handler

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"

	"gitlab.com/abhishp/stats-api/internal/server/response"
	"gitlab.com/abhishp/stats-api/internal/service"
	"gitlab.com/abhishp/stats-api/internal/service/contract"
)

type ErrorHandler func(err error) (httpStatus int, code int, message string)

func jsonResponse(w http.ResponseWriter, httpStatus int, data any) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(httpStatus)
	if data != nil {
		handleEncodingError(json.NewEncoder(w).Encode(data))
	}
}

func jsonErrorResponse(w http.ResponseWriter, err error) {
	httpStatus, code, message := handleCommonErrors(err)

	jsonResponse(w, httpStatus, response.NewErrorResponse(code, message))
}

func handleEncodingError(err error) {
	if err == nil {
		return
	}
	log.Printf("error during encoding: %v", err)
}

func handleCommonErrors(err error) (int, int, string) {
	if errors.Is(err, errNotAuthenticated) {
		return http.StatusUnauthorized, 401, err.Error()
	}
	requestError := badRequestError{}
	if errors.As(err, &requestError) {
		return http.StatusBadRequest, requestError.Code(), requestError.Error()
	}
	valError := contract.ValidationError{}
	if errors.As(err, &valError) {
		return http.StatusBadRequest, errCodeValidationFailed, valError.Error()
	}
	serviceError := service.Error{}
	if errors.As(err, &serviceError) {
		statusCode, isPresent := serviceErrorHTTPStatusCodeMap[serviceError]
		if !isPresent {
			statusCode = http.StatusUnprocessableEntity
		}
		return statusCode, serviceError.Code(), serviceError.Error()
	}

	return http.StatusInternalServerError, errCodeUnknownError, errMessageUnknownError
}

func parseBody(r *http.Request, data any) error {
	if r.Body == nil {
		return newBadRequestError(nil, errCodeMissingBody, "missing body")
	}
	defer func() { _ = r.Body.Close() }()

	jsonDecoder := json.NewDecoder(r.Body)
	jsonDecoder.DisallowUnknownFields()

	if err := jsonDecoder.Decode(data); err != nil {
		return newBadRequestError(err, errCodeBodyDecodeFailure, "decode body")
	}
	return nil
}

func getURLParam(r *http.Request, paramName string) (string, error) {
	vars := mux.Vars(r)
	if vars == nil || vars[paramName] == "" {
		return "", newBadRequestError(errors.New("missing url param"), errCodeMissingURLParam, "missing "+paramName+" in path")
	}

	return vars[paramName], nil
}

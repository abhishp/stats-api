package handler

import (
	"errors"
	"net/http"

	"gitlab.com/abhishp/stats-api/internal/service"
)

const (
	errCodeMissingBody       = 100
	errCodeBodyDecodeFailure = 101
	errCodeValidationFailed  = 102
	errCodeMissingURLParam   = 103
	errCodeInvalidURLParam   = 104
	errCodeInvalidQueryParam = 105

	errCodeUnknownError    = 500
	errMessageUnknownError = "something went wrong"
)

var serviceErrorHTTPStatusCodeMap = map[error]int{
	service.ErrUserNotFound:     http.StatusNotFound,
	service.ErrRecordNotFound:   http.StatusNotFound,
	service.ErrNoRecordsPresent: http.StatusPreconditionFailed,
}

var errNotAuthenticated = errors.New("not authenticated")

type badRequestError struct {
	cause error
	code  int
	msg   string
}

func newBadRequestError(cause error, code int, msg string) badRequestError {
	return badRequestError{cause: cause, code: code, msg: "bad request: " + msg}
}

func (b badRequestError) Error() string {
	if b.cause != nil {
		return b.msg + ": " + b.cause.Error()
	}
	return b.msg
}

func (b badRequestError) Cause() error {
	return b.cause
}

func (b badRequestError) Code() int {
	return b.code
}

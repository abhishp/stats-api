package handler_test

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/server/handler"
	"gitlab.com/abhishp/stats-api/internal/server/response"
	"gitlab.com/abhishp/stats-api/internal/service"
	"gitlab.com/abhishp/stats-api/internal/service/contract"
	"gitlab.com/abhishp/stats-api/internal/testing/mocks"
)

func TestStatsHandler_Index(t *testing.T) {
	newGetSummaryRequest := func(onContract, groupBy string) *http.Request {
		request, _ := http.NewRequest(http.MethodGet, "/stats/summary", nil)
		query := request.URL.Query()
		if onContract != "" {
			query.Set("on_contract", onContract)
		}
		if groupBy != "" {
			query.Set("group_by", groupBy)
		}
		request.URL.RawQuery = query.Encode()
		return request
	}
	boolQuery := func(val *bool) string {
		if val == nil {
			return ""
		}
		return fmt.Sprint(*val)
	}

	t.Run("success", func(t *testing.T) {
		testCases := map[string]*bool{
			"without on_contract query param":           nil,
			"with on_contract query param set to true":  lo.ToPtr(true),
			"with on_contract query param set to false": lo.ToPtr(false),
		}
		for name, onContract := range testCases {
			t.Run(name+" and without group by", func(t *testing.T) {
				mockStatsService := mocks.NewStatsService(t)
				statsHandler := handler.NewSummaryStatsHandler(mockStatsService)
				w, r := httptest.NewRecorder(), newGetSummaryRequest(boolQuery(onContract), "")

				summary := domain.SimplifiedSummary{
					Mean: 24.576,
					Min:  13,
					Max:  56,
				}
				mockStatsService.EXPECT().GetSalarySummary(r.Context(), contract.RecordSearchRequest{OnContract: onContract}).Return(summary, nil)

				statsHandler.Index(w, r)

				assert.Equal(t, http.StatusOK, w.Code)
				assert.JSONEq(t, `{"mean": 24.576, "min": 13, "max": 56}`, w.Body.String())
			})

			t.Run(name+" and grouped by department", func(t *testing.T) {
				mockStatsService := mocks.NewStatsService(t)
				statsHandler := handler.NewSummaryStatsHandler(mockStatsService)
				w, r := httptest.NewRecorder(), newGetSummaryRequest(boolQuery(onContract), "department")

				summary := map[string]domain.SimplifiedSummary{
					"Engineering":    {Mean: 50_096_257.5, Min: 30, Max: 200_000_000},
					"Operations":     {Mean: 35_015, Min: 30, Max: 70000},
					"Administration": {Mean: 30, Min: 30, Max: 30},
				}
				mockStatsService.EXPECT().GetSalarySummaryByDepartments(r.Context(), contract.RecordSearchRequest{OnContract: onContract}).Return(summary, nil)

				statsHandler.Index(w, r)

				assert.Equal(t, http.StatusOK, w.Code)
				summaryJSON, _ := json.Marshal(summary)
				assert.JSONEq(t, string(summaryJSON), w.Body.String())
			})

			t.Run(name+" and grouped by sub department", func(t *testing.T) {
				mockStatsService := mocks.NewStatsService(t)
				statsHandler := handler.NewSummaryStatsHandler(mockStatsService)
				w, r := httptest.NewRecorder(), newGetSummaryRequest(boolQuery(onContract), "sub_department")

				summary := map[string]map[string]domain.SimplifiedSummary{
					"Engineering": {
						"Platform": {Mean: 110_000, Min: 110_000, Max: 110_000},
					},
					"Banking": {
						"Loan": {Mean: 90_000, Min: 90_000, Max: 90_000},
					},
				}
				mockStatsService.EXPECT().GetSalarySummaryBySubDepartments(r.Context(), contract.RecordSearchRequest{OnContract: onContract}).Return(summary, nil)

				statsHandler.Index(w, r)

				assert.Equal(t, http.StatusOK, w.Code)
				summaryJSON, _ := json.Marshal(summary)
				assert.JSONEq(t, string(summaryJSON), w.Body.String())
			})
		}
	})

	t.Run("errors", func(t *testing.T) {
		t.Run("invalid query params", func(t *testing.T) {
			t.Run("on contract", func(t *testing.T) {
				mockStatsService := mocks.NewStatsService(t)
				statsHandler := handler.NewSummaryStatsHandler(mockStatsService)
				w, r := httptest.NewRecorder(), newGetSummaryRequest("foo-bar", "")

				statsHandler.Index(w, r)

				assert.Equal(t, http.StatusBadRequest, w.Code)
				assertErrorResponse(t, 105, "invalid value foo-bar for boolean query param on_contract", w.Body)
			})

			t.Run("group by", func(t *testing.T) {
				mockStatsService := mocks.NewStatsService(t)
				statsHandler := handler.NewSummaryStatsHandler(mockStatsService)
				w, r := httptest.NewRecorder(), newGetSummaryRequest("", "foo")

				statsHandler.Index(w, r)

				assert.Equal(t, http.StatusBadRequest, w.Code)
				assertErrorResponse(t, 105, "invalid grouping level foo", w.Body)
			})
		})

		t.Run("service errors", func(t *testing.T) {
			testCases := []struct {
				name     string
				status   int
				error    error
				response response.Response
			}{
				{name: "no records present", error: service.ErrNoRecordsPresent, status: http.StatusPreconditionFailed, response: response.NewErrorResponse(3, "no records present")},
				{name: "random error", error: errors.New("some stats service error"), status: http.StatusInternalServerError, response: response.NewErrorResponse(500, "something went wrong")},
			}
			for _, testCase := range testCases {
				t.Run(testCase.name, func(t *testing.T) {
					mockStatsService := mocks.NewStatsService(t)
					statsHandler := handler.NewSummaryStatsHandler(mockStatsService)
					w, r := httptest.NewRecorder(), newGetSummaryRequest("", "")

					mockStatsService.EXPECT().GetSalarySummary(r.Context(), contract.RecordSearchRequest{}).Return(domain.SimplifiedSummary{}, testCase.error)
					statsHandler.Index(w, r)

					assert.Equal(t, testCase.status, w.Code)
					assertErrorResponse(t, testCase.response.ErrorCode, testCase.response.Message, w.Body)
				})
			}
		})
	})
}

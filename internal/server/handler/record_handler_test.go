package handler_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gofrs/uuid"
	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/server/handler"
	"gitlab.com/abhishp/stats-api/internal/server/response"
	"gitlab.com/abhishp/stats-api/internal/service"
	"gitlab.com/abhishp/stats-api/internal/service/contract"
	"gitlab.com/abhishp/stats-api/internal/testing/factory"
	"gitlab.com/abhishp/stats-api/internal/testing/mocks"
)

func TestRecordHandler_Create(t *testing.T) {
	newCreateRecordRequest := func(req contract.CreateRecordRequest) *http.Request {
		reqJSON, _ := json.Marshal(req)
		request, _ := http.NewRequest(http.MethodPost, "/records", bytes.NewReader(reqJSON))
		return request
	}
	validReq := contract.CreateRecordRequest{
		Name:          "Guljit",
		Salary:        "30",
		Currency:      "USD",
		Department:    "Administration",
		SubDepartment: "Agriculture",
	}

	t.Run("success", func(t *testing.T) {
		mockRecordService := mocks.NewRecordService(t)
		recordHandler := handler.NewRecordHandler(mockRecordService)

		w, r := httptest.NewRecorder(), newCreateRecordRequest(validReq)

		record := factory.Records().Build()
		mockRecordService.EXPECT().Insert(r.Context(), validReq).Return(record, nil)

		recordHandler.Create(w, r)

		assert.Equal(t, http.StatusCreated, w.Code)
		resp := domain.Record{}
		assert.NoError(t, json.Unmarshal(w.Body.Bytes(), &resp))
		assert.Equal(t, record, resp)
	})

	t.Run("errors", func(t *testing.T) {
		t.Run("missing body", testMissingBodyError(handler.NewRecordHandler(nil).Create, http.MethodPost, "/records"))
		t.Run("invalid body", testBodyDecodeError(handler.NewRecordHandler(nil).Create, http.MethodPost, "/records"))

		t.Run("validation error", func(t *testing.T) {
			req := validReq
			req.Name = ""

			mockRecordService := mocks.NewRecordService(t)
			recordHandler := handler.NewRecordHandler(mockRecordService)
			w, r := httptest.NewRecorder(), newCreateRecordRequest(req)

			mockRecordService.EXPECT().Insert(r.Context(), req).Return(domain.Record{}, req.Validate())

			recordHandler.Create(w, r)

			assert.Equal(t, http.StatusBadRequest, w.Code)
			assertErrorResponse(t, 102, "Name: zero value", w.Body)
		})

		t.Run("service errors", func(t *testing.T) {
			mockRecordService := mocks.NewRecordService(t)
			recordHandler := handler.NewRecordHandler(mockRecordService)
			w, r := httptest.NewRecorder(), newCreateRecordRequest(validReq)

			serviceError := errors.New("some random record service error")
			mockRecordService.EXPECT().Insert(r.Context(), validReq).Return(domain.Record{}, serviceError)

			recordHandler.Create(w, r)

			assert.Equal(t, http.StatusInternalServerError, w.Code)
			assertUnknownError(t, w.Body)
		})
	})
}

func TestRecordHandler_Delete(t *testing.T) {
	newDeleteRecordRequest := func(id string) *http.Request {
		request, _ := http.NewRequest(http.MethodDelete, "/records/{id}", nil)
		if id == "" {
			return request
		}
		return mux.SetURLVars(request, map[string]string{"id": id})
	}
	t.Run("success", func(t *testing.T) {
		mockRecordService := mocks.NewRecordService(t)
		recordHandler := handler.NewRecordHandler(mockRecordService)

		recordID := uuid.Must(uuid.NewV4())
		w, r := httptest.NewRecorder(), newDeleteRecordRequest(recordID.String())

		mockRecordService.EXPECT().Delete(r.Context(), recordID).Return(nil)
		recordHandler.Delete(w, r)

		assert.Equal(t, http.StatusOK, w.Code)
		assertSuccessResponse(t, w.Body)
	})

	t.Run("errors", func(t *testing.T) {
		t.Run("url params", func(t *testing.T) {
			testCases := []struct {
				name     string
				id       string
				response response.Response
			}{
				{name: "missing id", id: "", response: response.Response{ErrorCode: 103, Message: "missing id in path"}},
				{name: "invalid id", id: "invalid-uuid", response: response.Response{ErrorCode: 104, Message: "invalid uuid format for record id"}},
			}
			for _, testCase := range testCases {
				t.Run(testCase.name, func(t *testing.T) {
					mockRecordService := mocks.NewRecordService(t)
					recordHandler := handler.NewRecordHandler(mockRecordService)

					w, r := httptest.NewRecorder(), newDeleteRecordRequest(testCase.id)

					recordHandler.Delete(w, r)

					assert.Equal(t, http.StatusBadRequest, w.Code)
					assertErrorResponse(t, testCase.response.ErrorCode, testCase.response.Message, w.Body)
				})
			}
		})
		t.Run("service errors", func(t *testing.T) {
			testCases := []struct {
				name     string
				error    error
				status   int
				response response.Response
			}{
				{name: "record not found", error: service.ErrRecordNotFound, status: http.StatusNotFound, response: response.NewErrorResponse(2, "record not found")},
				{name: "random error", error: errors.New("some record service error"), status: http.StatusInternalServerError, response: response.NewErrorResponse(500, "something went wrong")},
			}
			for _, testCase := range testCases {
				t.Run(testCase.name, func(t *testing.T) {
					mockRecordService := mocks.NewRecordService(t)
					recordHandler := handler.NewRecordHandler(mockRecordService)

					recordID := uuid.Must(uuid.NewV4())
					w, r := httptest.NewRecorder(), newDeleteRecordRequest(recordID.String())

					mockRecordService.EXPECT().Delete(r.Context(), recordID).Return(testCase.error)
					recordHandler.Delete(w, r)

					assert.Equal(t, testCase.status, w.Code)
					assertErrorResponse(t, testCase.response.ErrorCode, testCase.response.Message, w.Body)
				})
			}
		})
	})
}

func TestRecordHandler_List(t *testing.T) {
	newListRecordsRequest := func(onContract string) *http.Request {
		url := "/records"
		if onContract != "" {
			url = fmt.Sprintf("%s?on_contract=%s", url, onContract)
		}
		request, _ := http.NewRequest(http.MethodGet, url, nil)
		return request
	}
	t.Run("success", func(t *testing.T) {
		testCases := map[string]*bool{
			"without on_contract query param":           nil,
			"with on_contract query param set to true":  lo.ToPtr(true),
			"with on_contract query param set to false": lo.ToPtr(false),
		}
		for name, onContract := range testCases {
			t.Run(name, func(t *testing.T) {
				mockRecordService := mocks.NewRecordService(t)
				recordHandler := handler.NewRecordHandler(mockRecordService)
				records := factory.Records().BuildN(3)
				onContractQuery := ""
				if onContract != nil {
					onContractQuery = fmt.Sprint(*onContract)
				}
				w, r := httptest.NewRecorder(), newListRecordsRequest(onContractQuery)

				mockRecordService.EXPECT().List(r.Context(), contract.RecordSearchRequest{OnContract: onContract}).Return(records, nil)
				recordHandler.List(w, r)

				assert.Equal(t, http.StatusOK, w.Code)
				resp := make([]domain.Record, 0, 3)
				assert.NoError(t, json.Unmarshal(w.Body.Bytes(), &resp))
				assert.Equal(t, records, resp)
			})
		}
	})

	t.Run("errors", func(t *testing.T) {
		t.Run("invalid query params", func(t *testing.T) {
			mockRecordService := mocks.NewRecordService(t)
			recordHandler := handler.NewRecordHandler(mockRecordService)

			w, r := httptest.NewRecorder(), newListRecordsRequest("invalid-bool")

			recordHandler.List(w, r)

			assert.Equal(t, http.StatusBadRequest, w.Code)
			assertErrorResponse(t, 105, "invalid value invalid-bool for boolean query param on_contract", w.Body)
		})

		t.Run("service errors", func(t *testing.T) {
			mockRecordService := mocks.NewRecordService(t)
			recordHandler := handler.NewRecordHandler(mockRecordService)

			w, r := httptest.NewRecorder(), newListRecordsRequest("")

			err := errors.New("some record service error")
			mockRecordService.EXPECT().List(r.Context(), contract.RecordSearchRequest{}).Return(nil, err)
			recordHandler.List(w, r)

			assert.Equal(t, http.StatusInternalServerError, w.Code)
			assertUnknownError(t, w.Body)
		})
	})
}

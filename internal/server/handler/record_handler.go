package handler

import (
	"net/http"
	"strconv"

	"github.com/gofrs/uuid"

	"gitlab.com/abhishp/stats-api/internal/server/response"
	"gitlab.com/abhishp/stats-api/internal/service"
	"gitlab.com/abhishp/stats-api/internal/service/contract"
)

type RecordHandler struct {
	recordService service.RecordService
}

func NewRecordHandler(recordService service.RecordService) RecordHandler {
	return RecordHandler{recordService: recordService}
}

func (rh RecordHandler) Create(w http.ResponseWriter, r *http.Request) {
	request := contract.CreateRecordRequest{}
	if err := parseBody(r, &request); err != nil {
		jsonErrorResponse(w, err)
		return
	}
	record, err := rh.recordService.Insert(r.Context(), request)
	if err != nil {
		jsonErrorResponse(w, err)
		return
	}
	jsonResponse(w, http.StatusCreated, record)
}

func (rh RecordHandler) Delete(w http.ResponseWriter, r *http.Request) {
	id, err := getURLParam(r, "id")
	if err != nil {
		jsonErrorResponse(w, err)
		return
	}
	recordID, err := uuid.FromString(id)
	if err != nil {
		jsonErrorResponse(w, newBadRequestError(err, errCodeInvalidURLParam, "invalid uuid format for record id"))
		return
	}
	if err = rh.recordService.Delete(r.Context(), recordID); err != nil {
		jsonErrorResponse(w, err)
		return
	}
	jsonResponse(w, http.StatusOK, response.NewSuccessResponse())
}

func (rh RecordHandler) List(w http.ResponseWriter, r *http.Request) {
	request := contract.RecordSearchRequest{}
	if r.URL.Query().Has("on_contract") {
		onContractQuery := r.URL.Query().Get("on_contract")
		onContract, err := strconv.ParseBool(onContractQuery)
		if err != nil {
			jsonErrorResponse(w, newBadRequestError(err, errCodeInvalidQueryParam, "invalid value "+onContractQuery+" for boolean query param on_contract"))
			return
		}
		request.OnContract = &onContract
	}
	records, err := rh.recordService.List(r.Context(), request)
	if err != nil {
		jsonErrorResponse(w, err)
		return
	}
	jsonResponse(w, http.StatusOK, records)
}

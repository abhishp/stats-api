package handler_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/server/handler"
)

func TestHealthCheckHandler_IndexHandler(t *testing.T) {
	w := httptest.NewRecorder()
	r, err := http.NewRequest("GET", "/health-check", nil)
	assert.NoError(t, err)

	handler.HealthCheck(w, r)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.JSONEq(t, `{ "API": "OK" }`, w.Body.String())
}

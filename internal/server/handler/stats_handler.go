package handler

import (
	"net/http"
	"strconv"

	"gitlab.com/abhishp/stats-api/internal/service"
	"gitlab.com/abhishp/stats-api/internal/service/contract"
)

type SummaryStatsHandler struct {
	statsService service.StatsService
}

func NewSummaryStatsHandler(statsService service.StatsService) SummaryStatsHandler {
	return SummaryStatsHandler{statsService: statsService}
}

func (sh SummaryStatsHandler) Index(w http.ResponseWriter, r *http.Request) {
	filterParams := contract.RecordSearchRequest{}
	if r.URL.Query().Has("on_contract") {
		onContractQuery := r.URL.Query().Get("on_contract")
		onContract, err := strconv.ParseBool(onContractQuery)
		if err != nil {
			jsonErrorResponse(w, newBadRequestError(err, errCodeInvalidQueryParam, "invalid value "+onContractQuery+" for boolean query param on_contract"))
			return
		}
		filterParams.OnContract = &onContract
	}

	groupBy := r.URL.Query().Get("group_by")
	var summary any
	var err error
	switch groupBy {
	case "":
		summary, err = sh.statsService.GetSalarySummary(r.Context(), filterParams)
	case "department":
		summary, err = sh.statsService.GetSalarySummaryByDepartments(r.Context(), filterParams)
	case "sub_department":
		summary, err = sh.statsService.GetSalarySummaryBySubDepartments(r.Context(), filterParams)
	default:
		err = newBadRequestError(nil, errCodeInvalidQueryParam, "invalid grouping level "+groupBy)
	}

	if err != nil {
		jsonErrorResponse(w, err)
		return
	}
	jsonResponse(w, http.StatusOK, summary)
}

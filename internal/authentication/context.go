package authentication

import (
	"context"

	"github.com/pkg/errors"
)

type userClaimsKeyType struct{}

var userClaimsKey userClaimsKeyType

func claimsToContext(ctx context.Context, claims UserClaims) context.Context {
	return context.WithValue(ctx, userClaimsKey, claims)
}

func ClaimsFromContext(ctx context.Context) (UserClaims, error) {
	if claims, isOk := ctx.Value(userClaimsKey).(UserClaims); isOk {
		return claims, nil
	}
	return UserClaims{}, errors.Wrap(ErrAuthenticationFailure, "invalid user claims")
}

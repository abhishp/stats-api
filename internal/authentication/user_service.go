package authentication

import (
	"context"

	"gitlab.com/abhishp/stats-api/internal/domain"
)

type UserService interface {
	GetUserByUsername(ctx context.Context, username string) (domain.User, error)
}

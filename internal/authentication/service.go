package authentication

import (
	"context"
	"log"
	"time"

	"github.com/gofrs/uuid"
	"github.com/golang-jwt/jwt/v4"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/abhishp/stats-api/config"
)

type Service interface {
	Login(ctx context.Context, loginRequest LoginRequest) (string, *time.Time, error)
	Authenticate(ctx context.Context, tokenStr string) (UserClaims, error)
}

func NewService(userService UserService, authTokenConfig config.AuthTokenConfig) Service {
	return authService{
		authTokenConfig: authTokenConfig,
		userService:     userService,
	}
}

type authService struct {
	authTokenConfig config.AuthTokenConfig
	userService     UserService
}

func (as authService) Login(ctx context.Context, loginRequest LoginRequest) (string, *time.Time, error) {
	user, err := as.userService.GetUserByUsername(ctx, loginRequest.Username)
	if err != nil {
		log.Printf("failed to fetch user with username %s: %v\n", loginRequest.Username, err)
		return "", nil, ErrAuthenticationFailure
	}
	if err = bcrypt.CompareHashAndPassword([]byte(user.HashedPassword), []byte(loginRequest.Password)); err != nil {
		log.Printf("failed to compare hashed passwords for user#%s: %v\n", user.ID.String(), err)
		return "", nil, ErrAuthenticationFailure
	}
	tokenID, err := uuid.NewV4()
	if err != nil {
		log.Printf("failed to generate token uuid#%s\n", user.ID.String())
		return "", nil, ErrAuthenticationFailure
	}
	expirationTime := jwt.NewNumericDate(time.Now().Add(as.authTokenConfig.ValidityDuration).UTC())
	issuedAt := jwt.NewNumericDate(time.Now().UTC())
	claims := UserClaims{
		UserID:   user.ID,
		Username: user.Username,
		Name:     user.Name,
		RegisteredClaims: jwt.RegisteredClaims{
			Issuer:    "stats-api",
			Subject:   "user",
			Audience:  jwt.ClaimStrings{"web"},
			ExpiresAt: expirationTime,
			NotBefore: issuedAt,
			IssuedAt:  issuedAt,
			ID:        tokenID.String(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS512, claims)
	signedToken, err := token.SignedString(as.authTokenConfig.SecretKey)
	if err != nil {
		log.Printf("failed to sign token for user#%s: %v\n", user.ID.String(), err)
		return "", nil, ErrAuthenticationFailure
	}
	return signedToken, &expirationTime.Time, nil
}

func (as authService) Authenticate(_ context.Context, tokenStr string) (claims UserClaims, err error) {
	if tokenStr == "" {
		return claims, ErrAuthenticationFailure
	}
	token, err := jwt.ParseWithClaims(tokenStr, &claims, func(token *jwt.Token) (any, error) {
		return as.authTokenConfig.SecretKey, nil
	})
	if err != nil || !token.Valid {
		log.Printf("failed to parse JWT token(%s): %v\n", tokenStr, err)
		return UserClaims{}, ErrAuthenticationFailure
	}
	return claims, nil
}

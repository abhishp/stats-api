package authentication

import "gopkg.in/validator.v2"

type LoginRequest struct {
	Username string `json:"username" validate:"nonzero,min=5,max=20,regexp=^[a-z][a-zA-Z._]+$"`
	Password string `json:"password" validate:"nonzero,min=12,max=32"`
}

func (lr LoginRequest) Validate() error {
	return validator.Validate(lr)
}

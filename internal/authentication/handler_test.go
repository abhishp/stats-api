package authentication_test

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/samber/lo"
	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/authentication"
	"gitlab.com/abhishp/stats-api/internal/testing/mocks"
)

func TestHandler_Login(t *testing.T) {
	validRequest := func() *http.Request {
		request, _ := http.NewRequest(http.MethodPost, "/login", bytes.NewBufferString(`{"username": "jane.doe", "password": "some-super-secret-password"}`))
		return request
	}
	serviceRequest := authentication.LoginRequest{
		Username: "jane.doe",
		Password: "some-super-secret-password",
	}
	t.Run("success", func(t *testing.T) {
		mockAuthService := mocks.NewAuthenticationService(t)
		authHandler := authentication.NewHandler(mockAuthService)
		w, r := httptest.NewRecorder(), validRequest()

		authToken := "auth-token"
		mockAuthService.EXPECT().Login(r.Context(), serviceRequest).Return(authToken, lo.ToPtr(time.Now().Add(time.Minute)), nil)

		authHandler.Login(w, r)

		assert.Equal(t, http.StatusCreated, w.Code)
		assert.Equal(t, "application/json", w.Header().Get("Content-Type"))
		assert.JSONEq(t, `{"token": "auth-token"}`, w.Body.String())
	})

	t.Run("errors", func(t *testing.T) {
		t.Run("body error", func(t *testing.T) {
			t.Run("missing body", func(t *testing.T) {
				handler := authentication.NewHandler(nil)
				w := httptest.NewRecorder()
				r, _ := http.NewRequest(http.MethodPost, "/login", nil)

				handler.Login(w, r)

				assert.Equal(t, http.StatusBadRequest, w.Code)
				assert.Equal(t, "application/json", w.Header().Get("Content-Type"))
				assert.JSONEq(t, `{"error": "bad request", "message": "missing body", "success": false}`, w.Body.String())
			})

			t.Run("invalid json in body", func(t *testing.T) {
				handler := authentication.NewHandler(nil)
				w := httptest.NewRecorder()
				r, _ := http.NewRequest(http.MethodPost, "/login", strings.NewReader("{"))

				handler.Login(w, r)

				assert.Equal(t, http.StatusBadRequest, w.Code)
				assert.Equal(t, "application/json", w.Header().Get("Content-Type"))
				assert.JSONEq(t, `{"error": "bad request", "message": "unexpected EOF", "success": false}`, w.Body.String())
			})

			t.Run("validation error", func(t *testing.T) {
				mockAuthService := mocks.NewAuthenticationService(t)
				authHandler := authentication.NewHandler(mockAuthService)
				r, _ := http.NewRequest(http.MethodPost, "/login", strings.NewReader(`{"username": "SomeUsername", "password": "some-super-secret-password"}`))
				w := httptest.NewRecorder()

				authHandler.Login(w, r)

				assert.Equal(t, http.StatusBadRequest, w.Code)
				assert.Equal(t, "application/json", w.Header().Get("Content-Type"))
				assert.JSONEq(t, `{"error": "bad request", "message": "validation failure: Username: regular expression mismatch", "success": false}`, w.Body.String())
			})
		})

		t.Run("unauthorized", func(t *testing.T) {
			mockAuthService := mocks.NewAuthenticationService(t)
			authHandler := authentication.NewHandler(mockAuthService)
			w, r := httptest.NewRecorder(), validRequest()

			err := errors.Wrap(authentication.ErrAuthenticationFailure, "service error")
			mockAuthService.EXPECT().Login(r.Context(), serviceRequest).Return("", nil, err)

			authHandler.Login(w, r)

			assert.Equal(t, http.StatusUnauthorized, w.Code)
			assert.Equal(t, "application/json", w.Header().Get("Content-Type"))
		})
	})
}

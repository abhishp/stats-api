package authentication_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/authentication"
)

func TestLoginRequest_Validate(t *testing.T) {
	validPassword, validUsername := "Pc@p4Ejdk87KS7^3", "john.doe"
	t.Run("success", func(t *testing.T) {
		request := authentication.LoginRequest{Username: validUsername, Password: validPassword}

		err := request.Validate()

		assert.NoError(t, err)
	})

	t.Run("errors", func(t *testing.T) {
		t.Run("username", func(t *testing.T) {
			testCases := map[string]struct{ username, errorRegex string }{
				"missing":                          {"", "Username: zero value"},
				"less than 5 characters":           {"four", "Username: less than min"},
				"more than 20 characters":          {"more_than_twenty_characters", "Username: greater than max"},
				"does not start with small letter": {" foo_bar", "Username: regular expression mismatch"},
				"has invalid characters":           {"foo@bar", "Username: regular expression mismatch"},
			}
			for name, testCase := range testCases {
				t.Run(name, func(t *testing.T) {
					request := authentication.LoginRequest{Username: testCase.username, Password: validPassword}

					err := request.Validate()

					assert.Error(t, err)
					assert.Regexp(t, testCase.errorRegex, err.Error())
				})
			}
		})

		t.Run("password", func(t *testing.T) {
			testCases := map[string]struct{ password, errorRegex string }{
				"missing":                 {"", "Password: zero value"},
				"less than 12 characters": {"less_thn_12", "Password: less than min"},
				"more than 32 characters": {"more_than_thirty_two_characters_long", "Password: greater than max"},
			}
			for name, testCase := range testCases {
				t.Run(name, func(t *testing.T) {
					request := authentication.LoginRequest{Username: validUsername, Password: testCase.password}

					err := request.Validate()

					assert.Error(t, err)
					assert.Regexp(t, testCase.errorRegex, err.Error())
				})
			}
		})
	})
}

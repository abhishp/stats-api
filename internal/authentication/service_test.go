package authentication_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/abhishp/stats-api/config"
	"gitlab.com/abhishp/stats-api/internal/authentication"
	"gitlab.com/abhishp/stats-api/internal/domain"
	"gitlab.com/abhishp/stats-api/internal/service/repository"
	"gitlab.com/abhishp/stats-api/internal/testing/factory"
	"gitlab.com/abhishp/stats-api/internal/testing/mocks"
)

var authTokenConfig = config.AuthTokenConfig{
	ValidityDuration: 15 * time.Minute,
	SecretKey:        []byte("V@3*99QpjSb@Eb3_3d93c2ce_5N8uxTrRm^e"),
}

func TestNewAuthenticationService(t *testing.T) {
	authenticationService := authentication.NewService(mocks.NewUserService(t), authTokenConfig)

	assert.NotZero(t, authenticationService)
	assert.Implements(t, (*authentication.Service)(nil), authenticationService)
}

func TestAuthService_Login(t *testing.T) {
	ctx := context.Background()

	t.Run("success", func(t *testing.T) {
		mockUserService := mocks.NewUserService(t)
		authenticationService := authentication.NewService(mockUserService, authTokenConfig)

		password := gofakeit.Password(true, true, true, true, false, 20)
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 10)
		require.NoError(t, err)
		user := factory.Users().HashedPassword(string(hashedPassword)).Build()
		loginRequest := authentication.LoginRequest{Username: user.Username, Password: password}

		mockUserService.EXPECT().GetUserByUsername(ctx, user.Username).Return(user, nil)

		signedToken, expiresAt, err := authenticationService.Login(ctx, loginRequest)

		assert.NoError(t, err)
		loggedInUser, err := authenticationService.Authenticate(ctx, signedToken)
		assert.NoError(t, err)
		assert.Equal(t, user.ID, loggedInUser.UserID)
		assert.Equal(t, user.Name, loggedInUser.Name)
		assert.Equal(t, user.Username, loggedInUser.Username)
		assert.Equal(t, time.Now().Add(authTokenConfig.ValidityDuration).Unix(), expiresAt.Unix())
	})

	t.Run("error", func(t *testing.T) {
		t.Run("user not found", func(t *testing.T) {
			mockUserService := mocks.NewUserService(t)
			authenticationService := authentication.NewService(mockUserService, authTokenConfig)

			password := gofakeit.Password(true, true, true, true, false, 20)
			username := factory.Username()
			mockUserService.EXPECT().GetUserByUsername(ctx, username).Return(domain.User{}, repository.ErrNotFound)

			signedToken, expiresAt, err := authenticationService.Login(ctx, authentication.LoginRequest{Username: username, Password: password})

			assert.ErrorIs(t, err, authentication.ErrAuthenticationFailure)
			assert.Zero(t, signedToken)
			assert.Nil(t, expiresAt)
		})

		t.Run("random user repo error", func(t *testing.T) {
			mockUserService := mocks.NewUserService(t)
			authenticationService := authentication.NewService(mockUserService, authTokenConfig)

			password := gofakeit.Password(true, true, true, true, false, 20)
			username := factory.Username()

			userRepoError := errors.New("some user repo error")
			mockUserService.EXPECT().GetUserByUsername(ctx, username).Return(domain.User{}, userRepoError)

			signedToken, expiresAt, err := authenticationService.Login(ctx, authentication.LoginRequest{Username: username, Password: password})

			assert.ErrorIs(t, err, authentication.ErrAuthenticationFailure)
			assert.Zero(t, signedToken)
			assert.Nil(t, expiresAt)
		})

		t.Run("password comparison fails", func(t *testing.T) {
			mockUserService := mocks.NewUserService(t)
			authenticationService := authentication.NewService(mockUserService, authTokenConfig)

			password := gofakeit.Password(true, true, true, true, false, 20)
			user := factory.Users().Build()

			mockUserService.EXPECT().GetUserByUsername(ctx, user.Username).Return(user, nil)

			signedToken, expiresAt, err := authenticationService.Login(ctx, authentication.LoginRequest{Username: user.Username, Password: password})

			assert.ErrorIs(t, err, authentication.ErrAuthenticationFailure)
			assert.Zero(t, signedToken)
			assert.Nil(t, expiresAt)
		})
	})
}

func TestAuthService_Authenticate(t *testing.T) {
	ctx := context.Background()

	t.Run("success", func(t *testing.T) {
		mockUserService := mocks.NewUserService(t)
		authenticationService := authentication.NewService(mockUserService, authTokenConfig)

		password := gofakeit.Password(true, true, true, true, false, 20)
		hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 10)
		require.NoError(t, err)
		user := factory.Users().HashedPassword(string(hashedPassword)).Build()
		mockUserService.EXPECT().GetUserByUsername(ctx, user.Username).Return(user, nil)
		signedToken, _, err := authenticationService.Login(ctx, authentication.LoginRequest{Username: user.Username, Password: password})
		require.NoError(t, err)

		loggedInUser, err := authenticationService.Authenticate(ctx, signedToken)

		assert.NoError(t, err)
		assert.Equal(t, user.ID, loggedInUser.UserID)
		assert.Equal(t, user.Name, loggedInUser.Name)
		assert.Equal(t, user.Username, loggedInUser.Username)
	})

	t.Run("error", func(t *testing.T) {
		t.Run("empty token", func(t *testing.T) {
			mockUserService := mocks.NewUserService(t)
			authenticationService := authentication.NewService(mockUserService, authTokenConfig)

			loggedInUser, err := authenticationService.Authenticate(ctx, "")

			assert.ErrorIs(t, err, authentication.ErrAuthenticationFailure)
			assert.Zero(t, loggedInUser)
		})

		t.Run("invalid token", func(t *testing.T) {
			mockUserService := mocks.NewUserService(t)
			authenticationService := authentication.NewService(mockUserService, authTokenConfig)

			invalidToken := "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.pazba9Pj009HgANP4pTCQAHpXNU7pVbjIGff_plktSzsa9rXTGzFngaawzXGEO6Q0Hx5dtGi"
			loggedInUser, err := authenticationService.Authenticate(ctx, invalidToken)

			assert.ErrorIs(t, err, authentication.ErrAuthenticationFailure)
			assert.Zero(t, loggedInUser)
		})

		t.Run("expired token", func(t *testing.T) {
			mockUserService := mocks.NewUserService(t)
			authenticationService := authentication.NewService(mockUserService, authTokenConfig)

			invalidToken := "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJleHAiOjE1MTYyMzkwMjJ9.R2Dv_Ctsl-yeCz7eGFGGNvkh4DNh9lXjtzkJlpazjL1BaGhodcndQm_Hhkiar1vf-ztxrlbSMGn42HMLO8TeDw"
			loggedInUser, err := authenticationService.Authenticate(ctx, invalidToken)

			assert.ErrorIs(t, err, authentication.ErrAuthenticationFailure)
			assert.Zero(t, loggedInUser)
		})
	})
}

package authentication

import (
	"encoding/json"
	"log"
	"net/http"
)

type Handler struct {
	svc Service
}

func NewHandler(service Service) Handler {
	return Handler{svc: service}
}

func (h Handler) Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	loginRequest := LoginRequest{}
	if r.Body == nil {
		writeJSON(w, http.StatusBadRequest, json.RawMessage(`{"error": "bad request", "message": "missing body", "success": false}`))
		return
	}
	defer func() { _ = r.Body.Close() }()
	decoder := json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	if err := decoder.Decode(&loginRequest); err != nil {
		writeJSON(w, http.StatusBadRequest, map[string]any{"error": "bad request", "message": err.Error(), "success": false})
		return
	}
	if err := loginRequest.Validate(); err != nil {
		writeJSON(w, http.StatusBadRequest, map[string]any{"error": "bad request", "message": "validation failure: " + err.Error(), "success": false})
		return
	}
	token, expiresAt, err := h.svc.Login(r.Context(), loginRequest)
	if err != nil {
		writeJSON(w, http.StatusUnauthorized, nil)
		return
	}
	http.SetCookie(w, &http.Cookie{
		Name:    CookieName,
		Value:   token,
		Expires: *expiresAt,
	})
	writeJSON(w, http.StatusCreated, map[string]string{"token": token})
}

func writeJSON(w http.ResponseWriter, status int, data any) {
	w.WriteHeader(status)
	if data != nil {
		if err := json.NewEncoder(w).Encode(data); err != nil {
			log.Printf("error while encoding json response: %v\n", err)
		}
	}
}

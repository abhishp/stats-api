package authentication_test

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
	"github.com/gofrs/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/abhishp/stats-api/internal/authentication"
	"gitlab.com/abhishp/stats-api/internal/testing/matchers"
	"gitlab.com/abhishp/stats-api/internal/testing/mocks"
)

func TestNewAuthMiddleware(t *testing.T) {
	t.Run("success", func(t *testing.T) {
		t.Run("auth cookie present", func(t *testing.T) {
			mockAuthService := mocks.NewAuthenticationService(t)
			authMiddleware := authentication.NewMiddleware(mockAuthService)
			nextHandler := mocks.NewHTTPHandler(t)
			authToken := "rer-some-secret-auth-token"
			userClaims := authentication.UserClaims{
				UserID:   uuid.Must(uuid.NewV4()),
				Username: gofakeit.Username(),
			}
			r, _ := http.NewRequest(http.MethodGet, "/authenticated/url", nil)
			r.AddCookie(&http.Cookie{Name: authentication.CookieName, Value: authToken})
			w := httptest.NewRecorder()
			nextHandler.EXPECT().ServeHTTP(w, matchers.UserClaims(t, userClaims))

			mockAuthService.EXPECT().Authenticate(r.Context(), authToken).Return(userClaims, nil)

			authMiddleware.Middleware(nextHandler).ServeHTTP(w, r)
			nextHandler.AssertExpectations(t)
		})

		t.Run("authorization header", func(t *testing.T) {
			mockAuthService := mocks.NewAuthenticationService(t)
			authMiddleware := authentication.NewMiddleware(mockAuthService)
			nextHandler := mocks.NewHTTPHandler(t)
			authToken := "some-secret-auth-token"
			userClaims := authentication.UserClaims{
				UserID:   uuid.Must(uuid.NewV4()),
				Username: gofakeit.Username(),
			}
			r, _ := http.NewRequest(http.MethodGet, "/authenticated/url", nil)
			r.Header.Set("Authorization", "Bearer "+authToken)
			w := httptest.NewRecorder()
			nextHandler.EXPECT().ServeHTTP(w, matchers.UserClaims(t, userClaims))

			mockAuthService.EXPECT().Authenticate(r.Context(), authToken).Return(userClaims, nil)

			authMiddleware.Middleware(nextHandler).ServeHTTP(w, r)
			nextHandler.AssertExpectations(t)
		})
	})

	t.Run("error", func(t *testing.T) {
		t.Run("token not present in both cookie and header", func(t *testing.T) {
			mockAuthService := mocks.NewAuthenticationService(t)
			authMiddleware := authentication.NewMiddleware(mockAuthService)
			nextHandler := mocks.NewHTTPHandler(t)

			r, _ := http.NewRequest(http.MethodGet, "/authenticated/url", nil)
			w := httptest.NewRecorder()

			authMiddleware.Middleware(nextHandler).ServeHTTP(w, r)

			assert.Equal(t, http.StatusUnauthorized, w.Code)
			nextHandler.AssertExpectations(t)
		})

		t.Run("auth service error", func(t *testing.T) {
			mockAuthService := mocks.NewAuthenticationService(t)
			authMiddleware := authentication.NewMiddleware(mockAuthService)
			nextHandler := mocks.NewHTTPHandler(t)
			authToken := "some-secret-auth-token"

			r, _ := http.NewRequest(http.MethodGet, "/authenticated/url", nil)
			r.Header.Set("Authorization", "Bearer "+authToken)
			w := httptest.NewRecorder()
			mockAuthService.EXPECT().Authenticate(r.Context(), authToken).Return(authentication.UserClaims{}, errors.New("some auth service error"))

			authMiddleware.Middleware(nextHandler).ServeHTTP(w, r)

			assert.Equal(t, http.StatusUnauthorized, w.Code)
			nextHandler.AssertExpectations(t)
		})
	})
}

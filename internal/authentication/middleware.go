package authentication

import (
	"log"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

const (
	CookieName = "authToken"

	headerAuthorization = "Authorization"
	bearerPrefix        = "Bearer "
)

func NewMiddleware(authService Service) mux.MiddlewareFunc {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			cookie, err := r.Cookie(CookieName)
			tokenStr := ""
			if err != nil {
				tokenStr = strings.TrimPrefix(r.Header.Get(headerAuthorization), bearerPrefix)
			} else {
				tokenStr = cookie.Value
			}
			if len(tokenStr) == 0 {
				w.Header().Set("x-error", "missing token")
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
			userClaims, err := authService.Authenticate(r.Context(), tokenStr)
			if err != nil {
				log.Printf("authentication failure: %+v\n", err)
				w.Header().Set("x-error", "invalid token")
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(w, r.WithContext(claimsToContext(r.Context(), userClaims)))
		})
	}
}

package authentication

import "errors"

var (
	ErrAuthenticationFailure = errors.New("authentication failed")
)

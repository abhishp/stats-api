package authentication

import (
	"github.com/gofrs/uuid"
	"github.com/golang-jwt/jwt/v4"
)

type UserClaims struct {
	UserID   uuid.UUID `json:"userId"`
	Name     string    `json:"name,omitempty"`
	Username string    `json:"username"`
	jwt.RegisteredClaims
}

# force bash to use build-ins
SHELL := bash

PROJECT := gitlab.com/abhishp/stats-api
EXECUTABLE := out/bin/stats-api

GO ?= go
GOFILES		:= $(shell find . -type f -name '*.go' -not -path "./tmp/*")

export GO111MODULE ?= on
export GOPROXY ?= https://proxy.golang.org
export GOSUMDB ?= sum.golang.org
export GOBIN ?= $(GOPATH)/bin


help: ## Display this help
	@ echo "Please use \`make <target>' where <target> is one of:"
	@ echo
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
	@ echo

.DEFAULT_GOAL := help

clean: ## Clean artifacts
	@echo "cleaning build out"
	@rm -rf out && mkdir out

deps: ## Fetch dependencies
	$(GO) mod tidy

build: clean deps ## Build binary
	@echo "Building stats-api"
	@$(GO) build -mod=mod -o $(EXECUTABLE) ./cmd/stats-api

copy-config:
	@echo "Copying sample configs as config.yml"
	@cp config.sample.yml config.yml
	@cp users.sample.yml users.yml

server: build
	$(EXECUTABLE) -e development -c config.yml

test-unit: get-gotest ## Run unit tests
	gotest -shuffle=on -mod=mod -race -v -p=1 ./...

get-gotest:
ifeq (, $(shell which gotest))
	@go install github.com/rakyll/gotest@latest
endif

vet: ## Perform vet checks
	$(GO) vet ./...

lint: get-ci-lint ## Runs linting using golangci-lint
	@echo "Running lint tools..."
	$(GOBIN)/golangci-lint run --fix && $(GOBIN)/golangci-lint run

fmt: get-goimports ## Fix formatting (goimports)
	goimports -local $(PROJECT) -w $(GOFILES)

fmt-check: get-goimports ## Check formatting (goimports)
	goimports -e -d -l -local $(PROJECT) $(GOFILES)

generate-mocks: get-mockery
	mockery  --case=underscore --output="./internal/testing/mocks" --dir="./internal/service" --recursive=true --with-expecter --name="(.+Repository|.+Service)\z"
	@goimports -local $(PROJECT) -w internal/testing/mocks/*.go

get-ci-lint:
ifeq (, $(shell which golangci-lint))
	@echo "Installing golangci-lint command"
	@go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest;
endif

get-goimports:
ifeq (, $(shell which goimports))
	@echo "Installing goimports command"
	@go install golang.org/x/tools/cmd/goimports@latest
endif

get-mockery:
ifeq (, $(shell which mockery))
	@go install github.com/vektra/mockery/v2@latest
endif

